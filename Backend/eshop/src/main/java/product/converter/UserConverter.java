package product.converter;

import product.dto.request.LoginRequestDTO;
import product.dto.response.LoginResponseDTO;
import product.entity.User;

public class UserConverter {
    public static User toEntity(LoginRequestDTO loginRequestDTO){
        User user = new User();
        return user;
    }
    public static LoginResponseDTO toDTO(User user){
        LoginResponseDTO loginResponseDTO = new LoginResponseDTO();
        loginResponseDTO.setId(user.getId());
        loginResponseDTO.setToken(user.getToken());
        loginResponseDTO.setStatus(user.getStatus());
        loginResponseDTO.setRoles(user.getRoles());
        return loginResponseDTO;
    }
}
