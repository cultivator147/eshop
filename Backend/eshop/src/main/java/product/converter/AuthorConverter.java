package product.converter;

import product.dto.request.AuthorRequestDTO;
import product.dto.response.AuthorResponseDTO;
import product.entity.Author;

public class AuthorConverter {
    public static Author toEntity(long userId, AuthorRequestDTO authorRequestDTO){
        String name = authorRequestDTO.getName();
        int year = authorRequestDTO.getYear();
        String nationality = authorRequestDTO.getNationality();
        Author author = new Author(userId, name, year, nationality);
        return author;
    }
    public static AuthorResponseDTO toDTO(Author author){
        long id = author.getId();
        String name = author.getName();
        int year = author.getYear();
        String nationality = author.getNationality();
        AuthorResponseDTO authorResponseDTO = new AuthorResponseDTO(id, name, year, nationality);
        return authorResponseDTO;
    }
}
