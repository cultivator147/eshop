package product.converter;

import product.dto.request.CustomerProfileRequestDTO;
import product.dto.response.CustomerProfileResponseDTO;
import product.entity.CustomerProfile;

import java.sql.Date;

public class CustomerProfileConverter {
    public static CustomerProfile toEntity(long userId, CustomerProfileRequestDTO customerProfileRequestDTO){
        String name = customerProfileRequestDTO.getName();
        String avatar = customerProfileRequestDTO.getAvatar();
        String gender = customerProfileRequestDTO.getGender();
        String address = customerProfileRequestDTO.getAddress();
        String birthday = customerProfileRequestDTO.getBirthday();
        String interest = customerProfileRequestDTO.getInterest();
        String phoneNumber = customerProfileRequestDTO.getPhoneNumber();
        String email = customerProfileRequestDTO.getEmail();
        CustomerProfile customerProfile = new CustomerProfile(userId, name, avatar, gender, address, birthday, interest, phoneNumber, email);
        return customerProfile;
    }

    public static CustomerProfileResponseDTO toResponseDTO(long userId, CustomerProfile customerProfile) {
        String name = customerProfile.getName();
        String avatar = customerProfile.getAvatar();
        String gender = customerProfile.getGender();
        String address = customerProfile.getAddress();
        String birthday = customerProfile.getBirthday();
        String interest = customerProfile.getInterest();
        String phoneNumber = customerProfile.getPhoneNumber();
        String email = customerProfile.getEmail();
        CustomerProfileResponseDTO customerProfileResponseDTO = new CustomerProfileResponseDTO(userId, name, avatar, gender, address, birthday, interest, phoneNumber, email);
        return customerProfileResponseDTO;
    }
}
