package product.util;

import com.google.gson.JsonArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Logic {
    public static <T> List<T> paginate(List<T> list, int page, int pageSize) {
        int fromIndex = (page - 1) * pageSize;
        if (fromIndex >= list.size()) {
            return Collections.emptyList();
        }
        int toIndex = Math.min(fromIndex + pageSize, list.size());
        return list.subList(fromIndex, toIndex);
    }
    public static Long[] stringToArray(String s){
        if(s.length() == 2){
            return new Long[0];
        }
        s = s.replaceAll("[\\[\\]]", "");
        String[] arr = s.split(",");
        Long[] r = new Long[arr.length];
        for(int i = 0; i < arr.length; i++){
            r[i] = Long.parseLong(arr[i]);
        }
        return r;
    }
    public static List<Long> stringToList(String s){
        s = s.replaceAll("[\\[\\]]", "");
        String[] arr = s.split(", ");
        List<Long> r = new ArrayList<>();
        for(int i = 0; i < arr.length; i++){
            r.add(Long.parseLong(arr[i]));
        }
        return r;
    }
    public static JsonArray stringToJsonArray(String s){
        if(s.length() == 2){
            return new JsonArray();
        }
        s = s.replaceAll("[\\[\\]]", "");
        String[] arr = s.split(",");
        JsonArray r = new JsonArray();
        for(int i = 0; i<arr.length;i++){
            r.add(Long.parseLong(arr[i]));
        }
        return r;
    }
    public static String toConversationId(Long from, Long to){
        return from < to ? (from + "_" + to) : (to + "_" + from);
    }

}
