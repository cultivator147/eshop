package product.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;


@Configuration
public class SecurityConfig {

    private UserDetailsService userDetailsService;
    @Autowired
    JwtAuthenticationFilter jwtAuthenticationFilter;

    public SecurityConfig(UserDetailsService userDetailsService){
        this.userDetailsService = userDetailsService;
    }

    @Bean
    public static PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationManager authenticationManager(
            AuthenticationConfiguration configuration) throws Exception {
        return configuration.getAuthenticationManager();
    }

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

        http.csrf().disable()
                .authorizeHttpRequests((authorize) ->
                        //authorize.anyRequest().authenticated()
                        authorize
                                .requestMatchers(HttpMethod.POST,"/api/book/search").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/**").permitAll()
                                .requestMatchers(HttpMethod.POST,"/api/profile/customer/**").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/auth/**").permitAll()
                                .requestMatchers("/api/book/getbycategory").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/book/getrelatedbooks").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/book/getbyid").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/book/getcomments").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/book/getmycomments").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/book/getvotes").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/book/provider/delete").hasAuthority("ROLE_PROVIDER")
                                .requestMatchers("/api/book/getbycompany").permitAll()
                                .requestMatchers("/api/author/get").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/author/add").hasAuthority("ROLE_PROVIDER")
                                .requestMatchers("/api/author/update").hasAuthority("ROLE_PROVIDER")
                                .requestMatchers("/api/cart/updatebooksincart").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/cart/updatecartinfo").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/cart/getmycart").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/cart/getdeliveringcart").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/cart/getreceivedcart").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/cart/confirmcart").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/cart/getcanceledcart").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/cart/getwaitingcarts").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/cart/addbooktocart").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/cart/paycart").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/cart/deletefromcart").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/category/getall").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/customer/**").hasAuthority("ROLE_USER")
                                .requestMatchers("/api/book/add").hasAuthority(("ROLE_PROVIDER"))
                                .requestMatchers("/api/book/update").hasAuthority(("ROLE_PROVIDER"))
                                .requestMatchers("/api/provider/**").hasAuthority("ROLE_PROVIDER")
                                .requestMatchers("/api/company/**").hasAuthority("ROLE_PROVIDER")
                                .requestMatchers("/api/cart/cancelcart").permitAll()
                                .requestMatchers("/api/cart/provider/getdeliveringcart").hasAuthority("ROLE_PROVIDER")
                                .requestMatchers("/api/cart/provider/getcanceledcart").permitAll()
                                .requestMatchers("/api/cart/provider/getreceivedcart").hasAuthority("ROLE_PROVIDER")
                                .requestMatchers("/api/cart/provider/getwaitingcart").hasAuthority("ROLE_PROVIDER")
                                .requestMatchers("/api/cart/provider/acceptcart").hasAuthority("ROLE_PROVIDER")
                                .requestMatchers("/api/company/**").hasAuthority("ROLE_PROVIDER")
                                .requestMatchers("/api/message/send").permitAll()
                                .requestMatchers("/api/message/getall").permitAll()
                                .requestMatchers("/api/message/send").permitAll()



//                                .anyRequest().authenticated()
                );
        http.addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
        return http.build();
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }

    @Bean
    public FilterRegistrationBean<CorsFilter> corsFilterRegistrationBean() {
        FilterRegistrationBean<CorsFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(corsFilter());
        registrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return registrationBean;
    }


}
