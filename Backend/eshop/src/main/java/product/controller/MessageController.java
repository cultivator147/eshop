package product.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import product.entity.Message;
import product.service.message.MessageService;
import product.util.GsonUtil;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/api/message")
public class MessageController {
    @Autowired
    private MessageService messageService;
    @GetMapping("/getall")
    ResponseEntity getAllMessage(@RequestHeader(value="Authorization") String token){
        token = token.substring(7);
        List<Message> messages = messageService.getAllMessage(token);
        return ResponseEntity.ok(messages);
    }
    @PostMapping("/send")
    ResponseEntity sendMessage(@RequestHeader(value="Authorization") String token,@RequestBody String body){
        token = token.substring(7);
        JsonObject data = GsonUtil.toJsonObject(body);
        messageService.sendMessage(token, data);
        return ResponseEntity.ok().build();
    }
    @GetMapping("/getconversation")
    ResponseEntity getConversation(@RequestHeader(value="Authorization") String token, @RequestParam(value = "conversation_id") String conversationId){
        token = token.substring(7);
        JsonArray msgs = messageService.getConversation(token, conversationId);
        return ResponseEntity.ok(msgs.toString());
    }
}
