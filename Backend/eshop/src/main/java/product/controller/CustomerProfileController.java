package product.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import product.dto.request.CustomerProfileRequestDTO;
import product.dto.response.CustomerProfileResponseDTO;
import product.security.JwtAuthenticationFilter;
import product.service.customer.ProfileService;

@RestController
@RequestMapping("/api/profile")
@CrossOrigin
public class CustomerProfileController {
    @Autowired
    private ProfileService profileService;
    @Autowired
    private JwtAuthenticationFilter jwtAuthenticationFilter;

    @PostMapping("/customer/update")
    ResponseEntity updateProfile(HttpServletRequest request, @RequestBody CustomerProfileRequestDTO customerProfileRequestDTO){
        String token = jwtAuthenticationFilter.getTokenFromRequest(request);
        CustomerProfileResponseDTO customerProfileResponseDTO = profileService.updateProfile(token, customerProfileRequestDTO);
        return ResponseEntity.ok(customerProfileResponseDTO);
    }

    @GetMapping("/customer/get")
    ResponseEntity getProfile(HttpServletRequest request){
        String token = jwtAuthenticationFilter.getTokenFromRequest(request);
        CustomerProfileResponseDTO customerProfileResponseDTO = profileService.getProfile(token);
        return ResponseEntity.ok(customerProfileResponseDTO);
    }
}
