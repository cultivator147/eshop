package product.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import product.service.cart.CartService;
import product.util.GsonUtil;

import java.util.Objects;

@RestController
@RequestMapping("/api/cart")
public class CartController {
    @Autowired
    CartService cartService;
    @PutMapping("/updatebooksincart")
    ResponseEntity updateBooksInCart(@RequestHeader(value = "Authorization") String token, @RequestBody String body){
        token = token.substring(7);
        JsonObject data = GsonUtil.toJsonObject(body);
        JsonObject response = cartService.updateBooksInCart(token, Objects.requireNonNull(data));
        return ResponseEntity.status(HttpStatus.OK).body(response.toString());
    }
    @PostMapping("/updatecartinfo")
    ResponseEntity updateCartInfo(@RequestHeader(value = "Authorization") String token, @RequestBody String body){
        token = token.substring(7);
        JsonObject data = GsonUtil.toJsonObject(body);
        cartService.updateCartInfo(token, data);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    @GetMapping("/getmycart")
    ResponseEntity getMyCart(@RequestHeader(value = "Authorization") String token){
        token = token.substring(7);
        JsonObject cart = cartService.getMyCart(token);
        return ResponseEntity.ok(cart.toString());
    }
    @GetMapping("/getwaitingcarts")
    ResponseEntity getWaitingCarts(@RequestHeader(value = "Authorization") String token){
        token = token.substring(7);
        JsonArray cart = cartService.getWaitingCarts(token);
        return ResponseEntity.ok(cart.toString());
    }
    @GetMapping("/getdeliveringcart")
    ResponseEntity getDeliveringCart(@RequestHeader(value = "Authorization") String token){
        token = token.substring(7);
        JsonArray cart = cartService.getDeliveringCart(token);
        return ResponseEntity.ok(cart.toString());
    }
    @GetMapping("/getreceivedcart")
    ResponseEntity getReceivedCart(@RequestHeader(value = "Authorization") String token){
        token = token.substring(7);
        JsonArray cart = cartService.getReceivedCart(token);
        return ResponseEntity.ok(cart.toString());
    }
    @PostMapping("/addbooktocart")
    ResponseEntity addBookToCart(@RequestHeader(value = "Authorization") String token, @RequestBody String body){
        token = token.substring(7);
        JsonObject data = GsonUtil.toJsonObject(body);
        cartService.addBookToCart(token, data);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    @PutMapping("/paycart")
    ResponseEntity addBookToCart(@RequestHeader(value = "Authorization") String token){
        token = token.substring(7);
        cartService.payCart(token);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    @PutMapping("/cancelcart")
    ResponseEntity cancelCart(@RequestHeader(value = "Authorization") String token,@RequestBody String body){
        JsonObject data = GsonUtil.toJsonObject(body);
        token = token.substring(7);
        cartService.cancelCart(token, data.get("cart_id").getAsLong());
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    @PutMapping("/confirmcart")
    ResponseEntity confirmCart(@RequestHeader(value = "Authorization") String token,@RequestBody String body){
        JsonObject data = GsonUtil.toJsonObject(body);
        token = token.substring(7);
        cartService.confirmCart(token, data.get("cart_id").getAsLong());
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    @PutMapping("/deletefromcart")
    ResponseEntity deleteFromCart(@RequestHeader(value = "Authorization") String token, @RequestParam(value="book_id") long bookId){
        token = token.substring(7);
        cartService.deleteFromCart(token, bookId);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    @GetMapping("/getcanceledcart")
    ResponseEntity getCanceledCart(@RequestHeader(value = "Authorization") String token){
        token = token.substring(7);
        JsonArray carts = cartService.getCanceledCart(token);
        return ResponseEntity.ok(carts.toString());
    }

    @GetMapping("/provider/getdeliveringcart")
    ResponseEntity getDeliveringCartPvd(@RequestHeader(value = "Authorization") String token){
        token = token.substring(7);
        JsonArray carts = cartService.getDeliveringCartPvd(token);
        return ResponseEntity.ok(carts.toString());
    }
    @GetMapping("/provider/getwaitingcart")
    ResponseEntity getWaitingCart(@RequestHeader(value = "Authorization") String token){
        token = token.substring(7);
        JsonArray carts = cartService.getWaitingCartPvd(token);
        return ResponseEntity.ok(carts.toString());
    }
    @GetMapping("/provider/getcanceledcart")
    ResponseEntity getCanceledCartPvd(@RequestHeader(value = "Authorization") String token){
        token = token.substring(7);
        JsonArray carts = cartService.getCanceledCartPvd(token);
        return ResponseEntity.ok(carts.toString());
    }
    @GetMapping("/provider/getreceivedcart")
    ResponseEntity getReceivedCarts(@RequestHeader(value = "Authorization") String token){
        token = token.substring(7);
        JsonArray carts = cartService.getReceivedCartPvd(token);
        return ResponseEntity.ok(carts.toString());
    }
    @PostMapping("/provider/acceptcart")
    ResponseEntity acceptCart(@RequestHeader(value = "Authorization") String token,@RequestBody String body){
        JsonObject data = GsonUtil.toJsonObject(body);
        token = token.substring(7);
        cartService.acceptCart(token, data.get("cart_id").getAsLong());
        return ResponseEntity.ok().build();
    }
}
