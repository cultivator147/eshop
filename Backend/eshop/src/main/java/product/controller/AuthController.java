package product.controller;

import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;
import product.dto.request.LoginRequestDTO;
import product.dto.request.RegisterRequestDTO;
import product.dto.response.LoginResponseDTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import product.service.auth.AuthService;
import product.util.GsonUtil;

@RestController
@RequestMapping("/api/auth")
@CrossOrigin
public class AuthController {
    @Autowired
    private AuthService authService;
    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/login")
    ResponseEntity login(@RequestBody LoginRequestDTO user) {
        LoginResponseDTO loginResponseDTO = authService.login(user);
        return ResponseEntity.ok(loginResponseDTO);
    }
    @PostMapping("/register")
    ResponseEntity register(@RequestBody RegisterRequestDTO user) {
        LoginResponseDTO loginResponseDTO = authService.register(user);
        return new ResponseEntity(loginResponseDTO, HttpStatus.CREATED);
    }
    @DeleteMapping("/delete")
    void delete(@RequestBody String body){
        JsonObject data = GsonUtil.toJsonObject(body);
        long userid = data.get("userid").getAsLong();
        authService.deleteUser(userid);
    }

//    @GetMapping("/users")
//    List<User> getAllUsers() {
//    }

//    @GetMapping("/user/{id}")
//    User getUserById(@PathVariable Long id) {
//        return userRepository.findById(id)
//                .orElseThrow(() -> new UserNotFoundException(id));
//    }
}
