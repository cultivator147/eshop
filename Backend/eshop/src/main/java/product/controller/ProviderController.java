package product.controller;

import com.google.gson.JsonArray;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import product.entity.Provider;
import product.security.JwtAuthenticationFilter;
import product.service.provider.ProviderService;
import product.util.GsonUtil;

@RestController
@RequestMapping("/api/")
public class ProviderController {
    @Autowired
    private JwtAuthenticationFilter jwtAuthenticationFilter;
    @Autowired
    private ProviderService providerService;
    @PostMapping("provider/updateprofile")
    ResponseEntity updateProfile(HttpServletRequest request, @RequestBody String body){
        String token = jwtAuthenticationFilter.getTokenFromRequest(request);
        Provider provider = providerService.updateProfile(token, GsonUtil.toJsonObject(body));
        return ResponseEntity.ok(provider);
    }
    @GetMapping("provider/getprofile")
    ResponseEntity getProfile(HttpServletRequest request){
        String token = jwtAuthenticationFilter.getTokenFromRequest(request);
        Provider provider = providerService.getProfile(token);
        return ResponseEntity.ok(provider);
    }
    @GetMapping("getallproviders")
    ResponseEntity getAll(){
        JsonArray providerProfile = providerService.getAll();
        return ResponseEntity.ok(providerProfile.toString());
    }
}
