package product.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import jakarta.annotation.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import product.dto.request.BookRequestDTO;
import product.dto.response.CommentResponseDTO;
import product.dto.response.VoteResponseDTO;
import product.entity.Book;
import product.service.book.BookService;
import product.util.GsonUtil;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/book")
public class BookController {
    @Autowired
    private BookService bookService;
    @PostMapping("/add")
    ResponseEntity addBook(@RequestBody BookRequestDTO bookRequestDTO){
        bookService.addBook(bookRequestDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(bookRequestDTO);
    }
    @PutMapping("/update")
    ResponseEntity addBook(@RequestBody String body){
        JsonObject data = GsonUtil.toJsonObject(body);
        bookService.updateBook(data);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    @PostMapping("/search")
    ResponseEntity<List<Book>> searchBook(@RequestBody String data){
        JsonObject body = GsonUtil.toJsonObject(data);
        List<Book> books = bookService.searchBook(body);
        return ResponseEntity.status(HttpStatus.OK).body(books);
    }
    @GetMapping("/getbycategory")
    ResponseEntity<List<Book>> getByCategory(@RequestParam(value="category_id") Long categoryId){
       List<Book> books = bookService.getByCategory(categoryId);
        return ResponseEntity.status(HttpStatus.OK).body(books);
    }
    @GetMapping("/getrelatedbooks")
    ResponseEntity<List<Book>> getRelatedBooks(@RequestParam(value="book_id") Long bookId){
        List<Book> books = bookService.getRelatedBooks(bookId);
        return ResponseEntity.status(HttpStatus.OK).body(books);
    }
    @GetMapping("/getbyid")
    ResponseEntity getById(@RequestParam(value="book_id") Long bookId){
        Book book = bookService.getById(bookId);
        return ResponseEntity.status(HttpStatus.OK).body(book);
    }
    @GetMapping("/getcomments")
    ResponseEntity getComments(@RequestParam(value="book_id") Long bookId){
        List<CommentResponseDTO> comments = bookService.getComments(bookId);
        return ResponseEntity.ok(comments);
    }
    @GetMapping("/getmycomments")
    ResponseEntity getMyComments(@RequestHeader(value="Authorization") String token){
        token = token.substring(7);
        JsonArray comments = bookService.getMyComments(token);
        return ResponseEntity.ok(comments.toString());
    }
    @GetMapping("/getvotes")
    ResponseEntity getVotes(@RequestParam(value="book_id") Long bookId,@Nullable @RequestHeader(value = "Authorization") String token){
        VoteResponseDTO vote = bookService.getVotes(token, bookId);
        return ResponseEntity.ok(vote);
    }
    @GetMapping("/getbycompany")
    ResponseEntity<List<Book>> getByCompany(@RequestParam(value="company_id") Long companyId){
        List<Book> books = bookService.getByCompany(companyId);
        return ResponseEntity.ok(books);
    }
    @DeleteMapping("/provider/delete")
    ResponseEntity deleteBook(@RequestParam(value="book_id") Long bookId){
        bookService.deleteBook(bookId);
        return ResponseEntity.ok().build();
    }
    @GetMapping("/getbyprovider")
    ResponseEntity<List<Book>> getByProvider(@RequestParam(value="provider_id") Long providerId){
        List<Book> books = bookService.getByProvider(providerId);
        return ResponseEntity.ok(books);
    }
    @GetMapping("/getbyauthor")
    ResponseEntity<List<Book>> getByAuthor(@RequestParam(value="author_id") Long authorId){
        List<Book> books = bookService.getByAuthor(authorId);
        return ResponseEntity.ok(books);
    }
}
