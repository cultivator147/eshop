package product.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import product.entity.PublishingCompany;
import product.service.company.PublishingCompanyService;
import product.util.GsonUtil;

import java.util.List;

@RestController
@RequestMapping("/api")
public class PublishingCompaniesController {
    @Autowired
    private PublishingCompanyService publishingCompanyService;
    @GetMapping("/getallcompanies")
    ResponseEntity getAllCompanies(){
        List<PublishingCompany> companyList = publishingCompanyService.getAllCompanies();
        return ResponseEntity.ok(companyList);
    }
    @PostMapping("/company/addcompany")
    ResponseEntity addCompany(@RequestBody String body){
        JsonObject data =GsonUtil.toJsonObject(body);
        publishingCompanyService.addCompany(data);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    @PutMapping("/company/updatecompany")
    ResponseEntity updateCompany(@RequestBody String body){
        JsonObject data =GsonUtil.toJsonObject(body);
        publishingCompanyService.updateCompany(data);
        return ResponseEntity.ok().build();
    }
    @PutMapping("/company/deletecompany")
    ResponseEntity updateCompany(@RequestParam(value="company_id") Long companyId){
        publishingCompanyService.deleteCompany(companyId);
        return ResponseEntity.ok().build();
    }
}
