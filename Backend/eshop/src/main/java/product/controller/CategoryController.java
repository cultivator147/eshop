package product.controller;


import com.google.gson.JsonArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import product.entity.FatherCategory;
import product.service.category.CategoryService;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;
    @GetMapping("/getall")
    ResponseEntity getAllCategories(){
        JsonArray fatherCategories = categoryService.getAllCategories();
        return ResponseEntity.status(HttpStatus.OK).body(fatherCategories.toString());
    }

}
