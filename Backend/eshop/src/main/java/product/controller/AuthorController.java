package product.controller;


import jakarta.annotation.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import product.dto.request.AuthorRequestDTO;
import product.entity.Author;
import product.service.author.AuthorService;

import java.util.List;

@RestController
@RequestMapping("/api/author")
@CrossOrigin
public class AuthorController {
    @Autowired
    private AuthorService authorService;
    @PostMapping("/add")
    ResponseEntity addAuthor(@RequestBody AuthorRequestDTO authorRequestDTO){
        Author author = authorService.addAuthor(authorRequestDTO);
        return new ResponseEntity(author, HttpStatus.OK);
    }
    @PostMapping("/update")
    ResponseEntity updateAuthor(@RequestParam("authorId") long authorId, @RequestBody AuthorRequestDTO authorRequestDTO){
        Author author = authorService.updateAuthor(authorId, authorRequestDTO);
        return new ResponseEntity(author, HttpStatus.OK);
    }
    @GetMapping("/get")
    List<Author> getAuthor( ){
        List<Author> authors = authorService.getAuthorList();
        return authors;
    }


}
