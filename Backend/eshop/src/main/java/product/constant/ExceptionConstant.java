package product.constant;

public class ExceptionConstant {
    public static final String USERNAME_INVALID = "INVALID_USERNAME";
    public static final String PASSWORD_INVALID = "INVALID_PASSWORD";
    public static final String PASSWORD_CONFIRM_NOT_MATCH = "PASSWORD_CONFIRM_NOT_MATCH";
    public static final String USERNAME_EXIST = "USERNAME_EXIST";
    public static final String BOOK_NOT_EXISTED = "BOOK_NOT_EXISTED";
    public static final String PROFILE_NOT_EXISTED = "PROFILE_NOT_EXISTED";
    public static final String BOOK_NOT_FOUND = "BOOK_NOT_FOUND";
}
