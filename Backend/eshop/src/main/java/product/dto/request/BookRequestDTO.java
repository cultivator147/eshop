package product.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BookRequestDTO {
    private String name;
    private String image;
    private int numberOfPages;
    private String size;
    private int weight;
    private long price;
    private int year;
    private String introduction;
    private Set<Long> authors;
    private Set<Long> publishingCompanies;
    private Set<Long> categories;
    private Set<Long> providers;

}
