package product.dto.response;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CustomerProfileResponseDTO {
    private Long userId;
    private String name;

    private String avatar;
    private String gender;


    private String address;

    private String birthday;

    private String interest;

    private String phoneNumber;

    private String email;
}
