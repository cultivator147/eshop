package product.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VoteResponseDTO {
    private long bookId;
    private boolean isVoted;
    private int votedPoint;
    private double point;
   private int[] stars;

}
