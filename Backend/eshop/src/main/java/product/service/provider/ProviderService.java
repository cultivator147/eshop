package product.service.provider;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import product.entity.Provider;
import product.repository.ProviderProfileRepository;
import product.repository.UserRepository;
import product.security.JwtProvider;

import java.util.List;
import java.util.Optional;

@Service
public class ProviderService implements IProviderService {
    @Autowired
    private ProviderProfileRepository profileRepository;
    @Autowired
    private JwtProvider jwtProvider;
    @Autowired
    private UserRepository userRepository;

    @Override
    public Provider updateProfile(String token, JsonObject profile) {
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        Optional<Provider> prfOpt = profileRepository.findById(userId);
        if (prfOpt.isPresent()) {
            Provider prf = prfOpt.get();
            String name = prf.getName();
            String avatar = prf.getAvatar();
            String phoneNumber = prf.getPhoneNumber();
            String address = prf.getAddress();
            String tax = prf.getTax();
            String detailInfo = prf.getDetailInfo();
            String email = prf.getEmail();
            JsonElement e = profile.get("name");
            if (e != null) {
                name = e.getAsString();
            }
            e = profile.get("avatar");
            if (e != null) {
                avatar = e.getAsString();
            }
            e = profile.get("phone_number");
            if (e != null) {
                phoneNumber = e.getAsString();
            }
            e = profile.get("address");
            if (e != null) {
                address = e.getAsString();
            }
            e = profile.get("tax");
            if (e != null) {
                tax = e.getAsString();
            }
            e = profile.get("detail_info");
            if (e != null) {
                detailInfo = e.getAsString();
            }
            e = profile.get("email");
            if (e != null) {
                email = e.getAsString();
            }

            prf.setName(name);
            prf.setAvatar(avatar);
            prf.setAddress(address);
            prf.setEmail(email);
            prf.setTax(tax);
            prf.setPhoneNumber(phoneNumber);
            prf.setDetailInfo(detailInfo);
            profileRepository.save(prf);
            return prf;
        } else {
            throw new RuntimeException();
        }
    }

    @Override
    public JsonArray getAll() {
        List<Provider> providers = profileRepository.findAll();
        JsonArray r = new JsonArray();
        for(Provider prf : providers){
            long providerId = prf.getUserId();
            String providerName = prf.getName();
            String providerEmail = prf.getEmail();
            String providerAvatar = prf.getAvatar();
            String providerTax = prf.getTax();
            String providerAddress = prf.getAddress();
            String providerDetail = prf.getDetailInfo();
            long providerFollowers = prf.getFollowers();
            JsonObject js = new JsonObject();
            js.addProperty("provider_id", providerId);
            js.addProperty("email", providerEmail);
            js.addProperty("avatar", providerAvatar);
            js.addProperty("tax", providerTax);
            js.addProperty("address", providerAddress);
            js.addProperty("detail_info", providerDetail);
            js.addProperty("followers", providerFollowers);
            r.add(js);
        }
        return r;
    }

    public Provider getProfile(String token){
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        Optional<Provider> prfOpt = profileRepository.findById(userId);
        if (prfOpt.isPresent()) {
            return prfOpt.get();
        } else {
            Provider prf = new Provider();
            prf.setUserId(userId);
            profileRepository.save(prf);
            return prf;
        }
    }
}
