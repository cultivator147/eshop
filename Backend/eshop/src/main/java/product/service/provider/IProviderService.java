package product.service.provider;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import product.entity.Provider;

public interface IProviderService {
    Provider updateProfile(String token, JsonObject profile);

    JsonArray getAll();
}
