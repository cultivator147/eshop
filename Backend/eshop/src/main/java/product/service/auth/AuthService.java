package product.service.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import product.constant.ExceptionConstant;
import product.converter.UserConverter;
import product.dto.request.LoginRequestDTO;
import product.dto.request.RegisterRequestDTO;
import product.dto.response.LoginResponseDTO;
import product.entity.Role;
import product.entity.User;
import product.exception.EshopAPIException;
import product.repository.RoleRepository;
import product.repository.UserRepository;
import product.security.JwtProvider;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class AuthService implements IAuthService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    private AuthenticationManager authenticationManager;
    private PasswordEncoder passwordEncoder;
    private JwtProvider jwtProvider;

    public AuthService(
            JwtProvider jwtTokenProvider,
            UserRepository userRepository,
            PasswordEncoder passwordEncoder,
            AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtProvider = jwtTokenProvider;
    }

    public LoginResponseDTO login(LoginRequestDTO loginRequestDTO) {
        String username = loginRequestDTO.getUsername();
        String password = loginRequestDTO.getPassword();
        User user = userRepository.findByUsername(username);
        // Authenticate username and password
        if (user == null) {
            throw new EshopAPIException(HttpStatus.BAD_REQUEST, ExceptionConstant.USERNAME_INVALID);
        }
        //if OK:
        return login(username, password);
    }

    private LoginResponseDTO login(String username, String password) {
        User user = userRepository.findByUsername(username);
        //ERROR here
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(username, password)
        );
        String token = jwtProvider.generateToken(authentication);
        user.setToken(token);
        userRepository.save(user);
        LoginResponseDTO loginResponseDTO = UserConverter.toDTO(user);
        return loginResponseDTO;
    }

    public LoginResponseDTO register(RegisterRequestDTO registerRequestDTO) {
        String username = registerRequestDTO.getUsername();
        String password = registerRequestDTO.getPassword();
        String confirmPassword = registerRequestDTO.getConfirmPassword();
        if (!password.equals(confirmPassword)) {
            throw new EshopAPIException(HttpStatus.BAD_REQUEST, ExceptionConstant.PASSWORD_CONFIRM_NOT_MATCH);
        }
        User user = userRepository.findByUsername(username);
        if (user != null) {
            throw new EshopAPIException(HttpStatus.BAD_REQUEST, ExceptionConstant.USERNAME_EXIST);
        }

        String encodedPassword = passwordEncoder.encode(password);
        int status = 0;
        long now = System.currentTimeMillis();

        User userEntity = new User();
        userEntity.setUsername(username);
        userEntity.setPassword(encodedPassword);
        userEntity.setTokExpTime(now + 7 * 24 * 3600000);
        userEntity.setStatus(status);

        Role role;
        Optional<Role> roleOpt = roleRepository.findByName(registerRequestDTO.getRole());
        role = roleOpt.orElseGet(() -> roleRepository.findByName("ROLE_USER").get());

        Set<Role> roles = new HashSet<>();
        roles.add(role);
        userEntity.setRoles(roles);

        userRepository.save(userEntity);
        return login(username, password);
    }

    public void deleteUser(long userid) {
        userRepository.deleteUser(userid);
    }
}
