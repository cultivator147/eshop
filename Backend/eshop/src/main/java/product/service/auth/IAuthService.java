package product.service.auth;

import product.dto.request.LoginRequestDTO;
import product.dto.response.LoginResponseDTO;

public interface IAuthService {
    LoginResponseDTO login(LoginRequestDTO loginRequestDTO);
}
