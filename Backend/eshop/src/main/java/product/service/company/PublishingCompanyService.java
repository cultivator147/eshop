package product.service.company;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import product.constant.ExceptionConstant;
import product.entity.PublishingCompany;
import product.exception.EshopAPIException;
import product.repository.CompanyRepository;

import java.util.List;
import java.util.Optional;

@Service
public class PublishingCompanyService implements IPublishingCompanyService{
    @Autowired
    private CompanyRepository companyRepository;
    public List<PublishingCompany> getAllCompanies(){
        return companyRepository.getAll();
    }

    @Override
    public void addCompany(JsonObject data) {
        String name = data.get("name").getAsString();
        String address = data.get("address").getAsString();
        PublishingCompany company = new PublishingCompany();
        company.setAddress(address);
        company.setName(name);
        companyRepository.save(company);
    }
    public void updateCompany(JsonObject data){
        Long companyId = data.get("company_id").getAsLong();
        Optional<PublishingCompany> companyOpt = companyRepository.findById(companyId);
        if(companyOpt.isPresent()){
            PublishingCompany company = companyOpt.get();
            String name = company.getName();
            String address = company.getAddress();
            JsonElement e = data.get("name");
            if(e != null){
                name = e.getAsString();
            }
            e = data.get("address");
            if(e != null){
                address = e.getAsString();
            }
            company.setName(name);
            company.setAddress(address);
            companyRepository.save(company);
        }else{
            throw new EshopAPIException(HttpStatus.BAD_REQUEST, "COMPANY_NOT_FOUND");
        }
    }
    public void deleteCompany(long companyId){
        companyRepository.deleteCompany(companyId);
    }

}
