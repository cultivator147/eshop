package product.service.company;

import com.google.gson.JsonObject;
import product.entity.PublishingCompany;

import java.util.List;

public interface IPublishingCompanyService {
    List<PublishingCompany> getAllCompanies();

    void addCompany(JsonObject data);
}
