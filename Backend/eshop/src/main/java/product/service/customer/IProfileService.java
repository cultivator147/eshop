package product.service.customer;

import product.dto.request.CustomerProfileRequestDTO;
import product.dto.response.CustomerProfileResponseDTO;

public interface IProfileService {
    CustomerProfileResponseDTO updateProfile(CustomerProfileRequestDTO customerProfileRequestDTO);
}
