package product.service.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import product.converter.CustomerProfileConverter;
import product.dto.request.CustomerProfileRequestDTO;
import product.dto.response.CustomerProfileResponseDTO;
import product.entity.CustomerProfile;
import product.repository.ProfileRepository;
import product.repository.UserRepository;
import product.security.JwtProvider;

import java.util.Optional;

@Service
public class ProfileService {
    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private JwtProvider jwtProvider;
    @Autowired
    private UserRepository userRepository;
    public CustomerProfileResponseDTO updateProfile(String token, CustomerProfileRequestDTO customerProfileRequestDTO) {
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        Optional<CustomerProfile> prfOpt = profileRepository.findById(userId);
        if(prfOpt.isPresent()){
            CustomerProfile oldProfile = prfOpt.get();
            CustomerProfile newProfile = CustomerProfileConverter.toEntity(userId, customerProfileRequestDTO);
            if(newProfile.getAvatar() != null){
                oldProfile.setAvatar(newProfile.getAvatar());
            }
            if(newProfile.getName() != null){
                oldProfile.setName(newProfile.getName());
            }
            if(newProfile.getAddress() != null){
                oldProfile.setAddress(newProfile.getAddress());
            }
            if(newProfile.getEmail() != null){
                oldProfile.setEmail(newProfile.getEmail());
            }
            if(newProfile.getGender() != null){
                oldProfile.setGender(newProfile.getGender());
            }
            if(newProfile.getBirthday() != null){
                oldProfile.setBirthday(newProfile.getBirthday());
            }
            if(newProfile.getInterest() != null){
                oldProfile.setInterest(newProfile.getInterest());
            }
            if(newProfile.getPhoneNumber() != null){
                oldProfile.setPhoneNumber(newProfile.getPhoneNumber());
            }
            CustomerProfile customerProfile = profileRepository.save(oldProfile);
            CustomerProfileResponseDTO customerProfileResponseDTO = CustomerProfileConverter.toResponseDTO(userId, customerProfile);
            return customerProfileResponseDTO;
        }else{
            //Convert to Entity here
            CustomerProfile customerProfile = CustomerProfileConverter.toEntity(userId, customerProfileRequestDTO);
            customerProfile = profileRepository.save(customerProfile);
            //Convert to Response DTO here
            CustomerProfileResponseDTO customerProfileResponseDTO = CustomerProfileConverter.toResponseDTO(userId, customerProfile);
            return customerProfileResponseDTO;
        }
    }

    public CustomerProfileResponseDTO getProfile(String token) {
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        CustomerProfile customerProfile = profileRepository.findById(userId).get();
        CustomerProfileResponseDTO customerProfileResponseDTO = CustomerProfileConverter.toResponseDTO(userId, customerProfile);
        return customerProfileResponseDTO;

    }
}
