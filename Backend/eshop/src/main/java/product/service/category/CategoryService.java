package product.service.category;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import product.entity.Category;
import product.entity.FatherCategory;
import product.repository.CategoryRepository;
import product.repository.FatherCategoryRepository;
import product.util.GsonUtil;
import product.util.Logic;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService implements ICategoryService{
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private FatherCategoryRepository fatherCategoryRepository;
    public JsonArray getAllCategories(){
        JsonArray result = new JsonArray();
        List<FatherCategory> fatherCategories = fatherCategoryRepository.findAll();
        for(FatherCategory fatherCategory : fatherCategories){
            JsonObject js = new JsonObject();
            js.addProperty("category_name", fatherCategory.getName());
            JsonArray listCategories = new JsonArray();
            String categoriesString = fatherCategory.getCategories();
            Long[] categoriesId = Logic.stringToArray(categoriesString);
            for(int i = 0; i< categoriesId.length;i++){
                Optional<Category> categoryOpt = categoryRepository.findById(categoriesId[i]);
                if (categoryOpt.isPresent()) {
                    Category category = categoryOpt.get();
                    listCategories.add(GsonUtil.toJsonTree(category));
                }else{
                    listCategories.add(new JsonObject());
                }
            }
            js.add("list_categories", listCategories);
            result.add(js);
        }
        return result;
    }


}
