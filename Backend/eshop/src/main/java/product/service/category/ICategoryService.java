package product.service.category;

import com.google.gson.JsonArray;
import product.entity.FatherCategory;

import java.util.List;

public interface ICategoryService {
    JsonArray getAllCategories();
}
