package product.service.book;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import product.constant.ExceptionConstant;
import product.dto.request.BookRequestDTO;
import product.dto.response.CommentResponseDTO;
import product.dto.response.VoteResponseDTO;
import product.entity.*;
import product.exception.EshopAPIException;
import product.repository.*;
import product.security.JwtProvider;
import product.service.cart.CartService;
import product.util.GsonUtil;
import product.util.Logic;

import java.util.*;

@Service
public class BookService implements IBookService{
//    @Autowired
//    private CartService cartService;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private AuthorRepository authorRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private JwtProvider jwtProvider;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProviderProfileRepository providerProfileRepository;
    public void addBook(BookRequestDTO bookRequestDTO){
        String name = bookRequestDTO.getName();
        String image = bookRequestDTO.getImage();
        int numberOfPages = bookRequestDTO.getNumberOfPages();
        String size = bookRequestDTO.getSize();
        int weight = bookRequestDTO.getWeight();
        long price = bookRequestDTO.getPrice();
        int year = bookRequestDTO.getYear();
        String introduction = bookRequestDTO.getIntroduction();

        Book book = new Book();
        book.setName(name);
        book.setImage(image);
        book.setNumberOfPages(numberOfPages);
        book.setSize(size);
        book.setWeight(weight);
        book.setPrice(price);
        book.setYear(year);
        book.setIntroduction(introduction);

        Set<Long> publishingCompanies = bookRequestDTO.getPublishingCompanies();
        Set<PublishingCompany> booksCompany = new HashSet<>();

        publishingCompanies.forEach(element -> {
            if(companyRepository.findById(element).isPresent()){
                PublishingCompany publishingCompany = companyRepository.findById(element).get();
                booksCompany.add(publishingCompany);
            }
            else{
                throw new EshopAPIException(HttpStatus.BAD_REQUEST, "COMPANY_NOT_FOUND");
            }
        });


        Set<Long> authors = bookRequestDTO.getAuthors();
        Set<Author> bookAuthors = new HashSet<>();

        authors.forEach(element -> {
            if(authorRepository.findById(element).isPresent()){
                Author author = authorRepository.findById(element).get();
                bookAuthors.add(author);
            }else{
                throw new RuntimeException();
            }
        });

        Set<Long> categories = bookRequestDTO.getCategories();
        Set<Category> bookCategories = new HashSet<>();

        categories.forEach(element -> {
               if(categoryRepository.findById(element).isPresent()){
                    Category category = categoryRepository.findById(element).get();
                    bookCategories.add(category);
                }
            else{
                throw new EshopAPIException(HttpStatus.BAD_REQUEST, "CATEGORY_NOT_FOUND");

            }
        });

        Set<Long> providers = bookRequestDTO.getProviders();
        Set<Provider> bookProviders = new HashSet<>();

        providers.forEach(element -> {
            if(providerProfileRepository.findById(element).isPresent()){
                Provider provider = providerProfileRepository.findById(element).get();
                bookProviders.add(provider);
            }
            else{
                throw new EshopAPIException(HttpStatus.BAD_REQUEST, "PROVIDER_NOT_FOUND");

            }
        });
        book.setProviders(bookProviders);
        book.setPublishingCompanies(booksCompany);
        book.setCategories(bookCategories);
        book.setAuthors(bookAuthors);
        bookRepository.save(book);
    }

    @Override
    public List<Book> searchBook(JsonObject body) {
        String keySearch = body.get("keySearch").getAsString();
        int page;
        int pageSize;
        try{
            page = body.get("page").getAsInt();
        }catch (Exception e){
            page = 1;
        }
        try{
            pageSize = body.get("pageSize").getAsInt();
        }catch (Exception e){
            pageSize = 999;
        }
        List<Book> books = bookRepository.searchBooks(keySearch);
        books = Logic.paginate(books, page, pageSize);
        return books;
    }

    @Override
    public List<Book> getByCategory(long categoryId) {
        return bookRepository.findByCategoryId(categoryId);
    }

    @Override
    public Book getById(long bookId) {
        Optional<Book> bookOpt = bookRepository.findById(bookId);
        if(bookOpt.isPresent()){
            return bookOpt.get();
        }else{
            throw new EshopAPIException(HttpStatus.BAD_REQUEST, ExceptionConstant.BOOK_NOT_EXISTED);
        }
    }

    @Override
    public List<CommentResponseDTO> getComments(Long bookId) {
        List<CommentResponseDTO> commentResponseDTOS = new ArrayList<>();
        List<Comment> cmts = bookRepository.getComment(bookId);
        for(Comment cmt : cmts){
            Optional<CustomerProfile> customerProfileOpt = profileRepository.findById(cmt.getUserId());
            if(customerProfileOpt.isPresent()){
                CustomerProfile customerProfile = customerProfileOpt.get();
                CommentResponseDTO commentResponseDTO = new CommentResponseDTO(bookId, customerProfile.getName(), customerProfile.getAvatar(), cmt.getComment(), cmt.getTime());
                commentResponseDTOS.add(commentResponseDTO);
            }else{
                throw new EshopAPIException(HttpStatus.BAD_REQUEST, ExceptionConstant.PROFILE_NOT_EXISTED);
            }
        }
        return commentResponseDTOS;
    }

    @Override
    public VoteResponseDTO getVotes(String token, Long bookId) {
        VoteResponseDTO voteResponseDTO = new VoteResponseDTO();
        boolean isVoted = false;
        int votedPoint = 0;
        if(token != null){
            String username = jwtProvider.getUsername(token);
            long userId = userRepository.findByUsername(username).getId();
            Vote voteOpt = bookRepository.getVoted(userId, bookId);
            if(!voteOpt.equals(null)){
                isVoted = true;
                votedPoint = voteOpt.getVotePoint();
            }
        }
        List<Vote> votes = bookRepository.getVotes(bookId);
        double points = 0.00;
        int[] votedPoints = new int[6];
        for(Vote vote : votes){
            points += vote.getVotePoint();
            votedPoints[vote.getVotePoint()]++;
        }
        voteResponseDTO.setBookId(bookId);
        voteResponseDTO.setPoint(points/votes.size());
        voteResponseDTO.setStars(votedPoints);
        voteResponseDTO.setVoted(isVoted);
        voteResponseDTO.setVotedPoint(votedPoint);
        return voteResponseDTO;
    }

    @Override
    public JsonArray getMyComments(String token) {
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        JsonArray ar = new JsonArray();
        List<Comment> cmts = bookRepository.getMyComment(userId);
        for(Comment cmt : cmts){
            long bookId = cmt.getBookId();
            Vote vote = bookRepository.getVoted(userId, bookId);
            long startVoted = vote.getVotePoint();
            Optional<CustomerProfile> customerProfileOpt = profileRepository.findById(cmt.getUserId());
            if(customerProfileOpt.isPresent()){
                CustomerProfile customerProfile = customerProfileOpt.get();
                CommentResponseDTO commentResponseDTO = new CommentResponseDTO(cmt.getBookId(), customerProfile.getName(), customerProfile.getAvatar(), cmt.getComment(), cmt.getTime());
                JsonElement js = GsonUtil.toJsonTree(commentResponseDTO);
                js.getAsJsonObject().addProperty("starVoted", startVoted);
                ar.add(js);
            }else{
                throw new EshopAPIException(HttpStatus.BAD_REQUEST, ExceptionConstant.PROFILE_NOT_EXISTED);
            }
        }
        return ar;

    }

    public List<Book> getRelatedBooks(Long bookId) {
        Optional<Book> bookOPt = bookRepository.findById(bookId);
        if(bookOPt.isPresent()){
            List<Book> r = new ArrayList<>();
            Book book = bookOPt.get();
            Set<Category> cates =  book.getCategories();
            for(Category cate : cates){
                long cateId = cate.getId();
                List<Book> booksRelated = bookRepository.findByCategoryId(cateId);
                for(int i = 0; i < booksRelated.size();i++){
                    r.add(booksRelated.get(i));
                }
            }
            return bookRepository.searchBooks(book.getName().substring(1,4));
        }else{
            return new ArrayList<>();
        }
    }

    @Override
    public List<Book> getByCompany(Long companyId) {
        return bookRepository.findByCompanyId(companyId);
    }
    public List<Book> getByProvider(Long providerId){
        return bookRepository.findByProviderId(providerId);
    }
    public void updateBook(JsonObject data){
        long bookId = data.get("book_id").getAsLong();
        Optional<Book> bookOpt = bookRepository.findById(bookId);
        if(bookOpt.isPresent()){
            Book book = bookOpt.get();
            JsonElement e = data.get("name");
            if(e != null){
                String name = e.getAsString();
                book.setName(name);
            }
            e = data.get("image");
            if(e != null){
                String image = e.getAsString();
                book.setImage(image);
            }
            e = data.get("number_of_pages");
            if(e != null){
                int numberOfPages = e.getAsInt();
                book.setNumberOfPages(numberOfPages);
            }
            e = data.get("size");
            if(e != null){
                String size = e.getAsString();
                book.setSize(size);
            }
            e = data.get("weight");
            if(e != null){
                int weight = e.getAsInt();
                book.setWeight(weight);
            }
            e = data.get("price");
            if(e != null){
                long price = e.getAsLong();
                book.setPrice(price);
            }
            e = data.get("year");
            if(e != null){
                int year = e.getAsInt();
                book.setYear(year);
            }
            e = data.get("introduction");
            if(e != null){
                String introduction = e.getAsString();
                book.setIntroduction(introduction);
            }
            e = data.get("authors");
            if(e != null){
                Set<Author> authorsSet = new HashSet<>();
                JsonArray authorsId = e.getAsJsonArray();
                for(int i = 0; i<authorsId.size(); i++){
                    long authorId = authorsId.get(i).getAsLong();
                    Optional<Author> author = authorRepository.findById(authorId);
                    if(author.isPresent()){
                        authorsSet.add(author.get());
                    }else{
                        throw new EshopAPIException(HttpStatus.BAD_REQUEST, "AUTHOR_NOT_FOUND");
                    }
                }
                book.setAuthors(authorsSet);
            }
            e = data.get("publishing_companies");
            if(e != null){
                Set<PublishingCompany> companySet = new HashSet<>();
                JsonArray companiesId = e.getAsJsonArray();
                for(int i = 0; i<companiesId.size(); i++){
                    long companyId = companiesId.get(i).getAsLong();
                    Optional<PublishingCompany> company = companyRepository.findById(companyId);
                    if(company.isPresent()){
                        companySet.add(company.get());
                    }else{
                        throw new EshopAPIException(HttpStatus.BAD_REQUEST, "COMPANY_NOT_FOUND");
                    }
                }
                book.setPublishingCompanies(companySet);
            }
            e = data.get("categories");
            if(e != null){
                Set<Category> categorySet = new HashSet<>();
                JsonArray categoriesId = e.getAsJsonArray();
                for(int i = 0; i<categoriesId.size(); i++){
                    long categoryId = categoriesId.get(i).getAsLong();
                    Optional<Category> category = categoryRepository.findById(categoryId);
                    if(category.isPresent()){
                        categorySet.add(category.get());
                    }else{
                        throw new EshopAPIException(HttpStatus.BAD_REQUEST, "CATEGORY_NOT_FOUND");
                    }
                }
                book.setCategories(categorySet);
            }
            e = data.get("providers");
            if(e != null){
                Set<Provider> providerSet = new HashSet<>();
                JsonArray providersId = e.getAsJsonArray();
                for(int i = 0; i<providersId.size(); i++){
                    long providerId = providersId.get(i).getAsLong();
                    Optional<Provider> provider = providerProfileRepository.findById(providerId);
                    if(provider.isPresent()){
                        providerSet.add(provider.get());
                    }else{
                        throw new EshopAPIException(HttpStatus.BAD_REQUEST, "PROVIDER_NOT_FOUND");
                    }
                }
                book.setProviders(providerSet);
            }
            bookRepository.save(book);

        }
    }

    @Override
    public void deleteBook(Long bookId) {
        bookRepository.deleteBook(bookId);
//        Optional<Book> bookOpt = bookRepository.findById(bookId);
//        if(bookOpt.isPresent()){
//            Book book = bookOpt.get();
//            Set<PublishingCompany> cps = book.getPublishingCompanies();
//            for(PublishingCompany cp : cps){
//                long cpId = cp.getId();
//                companyRepository.deleteById(cpId);
//            }
//
//            Set<Category> cates = book.getCategories();
//            for(Category c : cates){
//                long cId = c.getId();
//                categoryRepository.deleteById(cId);
//            }
//
//            Set<Author> authors = book.getAuthors();
//            for(Author a : authors){
//                long authorId = a.getId();
//                authorRepository.deleteById(authorId);
//            }
//            bookRepository.deleteById(bookId);
//        }else{
//            throw new EshopAPIException(HttpStatus.BAD_REQUEST, "BOOK_NOT_FOUND");
//        }
    }

    @Override
    public List<Book> getByAuthor(Long authorId) {
        return bookRepository.findByAuthorId(authorId);
    }
}
