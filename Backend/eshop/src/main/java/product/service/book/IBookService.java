package product.service.book;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import product.dto.request.BookRequestDTO;
import product.dto.response.CommentResponseDTO;
import product.dto.response.VoteResponseDTO;
import product.entity.Book;

import java.util.List;

public interface IBookService {
    void addBook(BookRequestDTO bookRequestDTO);
    List<Book> searchBook(JsonObject body);

    List<Book> getByCategory(long categoryId);

    Book getById(long bookId);

    List<CommentResponseDTO> getComments(Long bookId);

    VoteResponseDTO getVotes(String token, Long bookId);

    JsonArray getMyComments(String token);
    List<Book> getRelatedBooks(Long bookId);

    List<Book> getByCompany(Long companyId);

    void deleteBook(Long bookId);

    List<Book> getByAuthor(Long authorId);
}
