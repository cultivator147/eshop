package product.service.cart;


import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import product.constant.ExceptionConstant;
import product.entity.*;
import product.exception.EshopAPIException;
import product.repository.CartRepository;
import product.repository.ProfileRepository;
import product.repository.UserRepository;
import product.security.JwtProvider;
import product.service.book.BookService;
import product.util.GsonUtil;
import product.util.Logic;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class CartService implements ICartService {
    private static final int CART_RECEIVED_STATUS = 3;
    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private JwtProvider jwtProvider;
    @Autowired
    BookService bookService;
    @Autowired
    CartRepository cartRepository;

    @Override
    public JsonObject updateBooksInCart(String token, JsonObject data) {
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        long bookToChange = data.get("book_id").getAsLong();
        long quantity = data.get("quantity").getAsLong();
        Optional<Cart> cartOpt = cartRepository.getMyCart(userId);
        if (cartOpt.isPresent()) {
            Cart cart = cartOpt.get();
            long totalPrice = 0;
            long discountPrice = 0;
            // ======= handle voucher under ============
            //TODO
            // ======= handle voucher above ============
            JsonObject myCart = new JsonObject();
            myCart.addProperty("address", cart.getAddress());
            JsonArray booksJsArr = Logic.stringToJsonArray(cart.getBooks());
            JsonArray quantitiesJsArr = Logic.stringToJsonArray(cart.getQuantity());
            for (int i = 0; i < booksJsArr.size(); i++) {
                Book bookEnt = bookService.getById(booksJsArr.get(i).getAsLong());
                long bookId = bookEnt.getId();
                if (bookId == bookToChange) {
                    if (quantity == 0) {
                        booksJsArr.remove(i);
                    } else {
                        long[] quantitiesArr = GsonUtil.toLongArray(quantitiesJsArr);
                        quantitiesArr[i] = quantity;
                        quantitiesJsArr = GsonUtil.toJsonArray(quantitiesArr);
                    }
                }
            }
            cart.setQuantity(quantitiesJsArr.toString());
            cart.setBooks(booksJsArr.toString());
            cartRepository.save(cart);
            return getMyCart(token);
        } else {
            getMyCart(token);
            return new JsonObject();
        }


    }


    @Override
    public void addBookToCart(String token, JsonObject data) {
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        Long bookId = data.get("bookId").getAsLong();
        Optional<Cart> cartOpt = cartRepository.getMyCart(userId);
        Cart cart = new Cart();
        if (!cartOpt.isEmpty()) {
            cart = cartOpt.get();
            JsonArray booksId = Logic.stringToJsonArray(cart.getBooks());
            JsonArray quantitiesJs = Logic.stringToJsonArray(cart.getQuantity());
            long[] quantities = GsonUtil.toLongArray(quantitiesJs);
            boolean isExisted = false;
            for (int i = 0; i < booksId.size(); i++) {
                if (booksId.get(i).getAsLong() == bookId) {
                    quantities[i]++;
                    isExisted = true;
                    break;
                }
            }
            quantitiesJs = GsonUtil.toJsonArray(quantities);
            if (!isExisted) {
                booksId.add(bookId);
                quantitiesJs.add(1L);
            }
            cart.setBooks(booksId.toString());
            cart.setQuantity(quantitiesJs.toString());
        } else {
            JsonArray books = new JsonArray(1);
            books.add(bookId);
            JsonArray quantities = new JsonArray(1);
            quantities.add(1);
            cart.setBooks(books.toString());
            cart.setQuantity(quantities.toString());
            cart.setStatus(0);
            cart.setCustomerId(userId);
        }
        cartRepository.save(cart);
    }
    @Override
    public void deleteFromCart(String token, long bookId) {
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        Optional<Cart> cartOpt = cartRepository.getMyCart(userId);
        if (cartOpt.isPresent()) {
            Cart cart = cartOpt.get();
            JsonArray booksId = Logic.stringToJsonArray(cart.getBooks());
            JsonArray quantitiesJs = Logic.stringToJsonArray(cart.getQuantity());
            for (int i = 0; i < booksId.size(); i++) {
                if (booksId.get(i).getAsLong() == bookId) {
                    booksId.remove(i);
                    quantitiesJs.remove(i);
                    break;
                }
            }
            cart.setBooks(booksId.toString());
            cart.setQuantity(quantitiesJs.toString());
            cartRepository.save(cart);
        } else {
            throw new EshopAPIException(HttpStatus.BAD_REQUEST, ExceptionConstant.BOOK_NOT_FOUND);
        }
    }


    @Override
    public void updateCartInfo(String token, JsonObject data) {
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        String newAddress = "";
        String phone = "";
        String userInfo = "";
        JsonElement e = data.get("address");
        if (e != null) {
            newAddress = e.getAsString();
        }
        e = data.get("phone_number");
        if (e != null) {
            phone = e.getAsString();
        }
        e = data.get("user_info");
        if (e != null) {
            userInfo = e.getAsString();
        }

        Optional<Cart> cartOpt = cartRepository.getMyCart(userId);
        if (cartOpt.isPresent()) {
            Cart cart = cartOpt.get();
            cart.setAddress(newAddress);
            cart.setPhoneNumber(phone);
            cart.setUserInfo(userInfo);
            cartRepository.save(cart);
        }
    }


    @Override
    public void payCart(String token) {
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        cartRepository.payCart(userId);
    }

    //BOTH 2
    @Override
    public void cancelCart(String token, long cartId) {
        String username = jwtProvider.getUsername(token);
        User user = userRepository.findByUsername(username);
        Set<Role> roles = user.getRoles();
        for(Role r : roles){
            if(r.getName().equals("ROLE_USER")){
                long userId = user.getId();
                Optional<Cart> cartOpt =  cartRepository.findById(cartId);
                if(cartOpt.isPresent()){
                    Cart cart = cartOpt.get();
                    if(cart.getCustomerId() != userId){
                        throw new EshopAPIException(HttpStatus.BAD_REQUEST, "CART_NOT_YOURS");
                    }
                    if(cart.getStatus() == CART_RECEIVED_STATUS){
                        throw new EshopAPIException(HttpStatus.BAD_REQUEST, "WRONG_STATUS");
                    }
                }else{
                    throw new EshopAPIException(HttpStatus.BAD_REQUEST,"CART_NOT_FOUND");
                }
            }else if(r.getName().equals("ROLE_PROVIDER")){
                Optional<Cart> cartOpt =  cartRepository.findById(cartId);
                if(cartOpt.isPresent()) {
                    Cart cart = cartOpt.get();
                    if(cart.getStatus() == CART_RECEIVED_STATUS){
                        throw new EshopAPIException(HttpStatus.BAD_REQUEST, "WRONG_STATUS");
                    }
                }else{
                    throw new EshopAPIException(HttpStatus.BAD_REQUEST,"CART_NOT_FOUND");
                }
            }else{
                throw new EshopAPIException(HttpStatus.BAD_REQUEST,"NO_ROLE_FOUND");
            }
        }
        cartRepository.cancelCart(cartId);
    }



    @Override
    public JsonObject getMyCart(String token) {
        try {
            String username = jwtProvider.getUsername(token);
            long userId = userRepository.findByUsername(username).getId();
            Optional<CustomerProfile> customerProfileOpt = profileRepository.findById(userId);
            String phoneNumber = "";
            String address = "";
            if (customerProfileOpt.isPresent()) {
                CustomerProfile customerProfile = customerProfileOpt.get();
                phoneNumber = customerProfile.getPhoneNumber();
                address = customerProfile.getAddress();
            }
            Optional<Cart> cartOpt = cartRepository.getMyCart(userId);
            if (cartOpt.isPresent()) {
                Cart cart = cartOpt.get();
                long totalPrice = 0;
                long discountPrice = 0;
                // ======= handle voucher under ============
                //TODO
                // ======= handle voucher above ============
                JsonObject myCart = new JsonObject();
                myCart.addProperty("cartId", cart.getId());
                myCart.addProperty("address", cart.getAddress());
                JsonArray booksCart = new JsonArray();
                JsonArray books = Logic.stringToJsonArray(cart.getBooks());
                JsonArray quantities = Logic.stringToJsonArray(cart.getQuantity());
                for (int i = 0; i < books.size(); i++) {
                    JsonObject book = new JsonObject();
                    Book bookEnt = bookService.getById(books.get(i).getAsLong());
                    book.addProperty("bookId", books.get(i).getAsLong());
                    book.addProperty("bookName", bookEnt.getName());
                    book.addProperty("bookAvatar", bookEnt.getImage());
                    book.addProperty("bookPrice", bookEnt.getPrice());
                    book.addProperty("quantity", quantities.get(i).getAsLong());
                    totalPrice += bookEnt.getPrice() * quantities.get(i).getAsLong();
                    booksCart.add(book);
                }
                myCart.add("items", booksCart);
                myCart.addProperty("totalPrice", totalPrice);
                myCart.addProperty("discountPrice", discountPrice);
                return myCart;
            } else {
                Cart cart = new Cart();
                cart.setCustomerId(userId);
                cart.setAddress(address);
                cart.setPhoneNumber(phoneNumber);
                cart.setBooks(new JsonArray().toString());
                cart.setQuantity(new JsonArray().toString());
                cartRepository.save(cart);
                return GsonUtil.toJsonTree(cart).getAsJsonObject();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }
    public JsonArray getWaitingCarts(String token){
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        List<Cart> carts = cartRepository.getWaitingCarts(userId);
        return getCart(carts);
    }
    public JsonArray getDeliveringCart(String token){
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        List<Cart> carts = cartRepository.getDeliveringCart(userId);
        return getCart(carts);
    }
    public JsonArray getCanceledCart(String token) {
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        List<Cart> carts = cartRepository.getCanceledCart(userId);
       return getCart(carts);
    }
    @Override
    public JsonArray getReceivedCart(String token) {
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        List<Cart> carts = cartRepository.getReceivedCart(userId);
        return getCart(carts);
    }

    //Provider
    @Override
    public JsonArray getWaitingCartPvd(String token) {
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        List<Cart> carts = cartRepository.getWaitingCartsPvd();
        return getCart(carts);
    }
    @Override
    public JsonArray getReceivedCartPvd(String token) {
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        List<Cart> carts = cartRepository.getReceivedCartPvd();
        return getCart(carts);
    }
    @Override
    public JsonArray getCanceledCartPvd(String token) {
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        List<Cart> carts = cartRepository.getCanceledCartPvd();
        return getCart(carts);
    }

    @Override
    public void acceptCart(String token, long cartId) {
        cartRepository.acceptCart(cartId);
    }

    @Override
    public void confirmCart(String token, long cartId) {
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        cartRepository.confirmCart(cartId);
    }

    @Override
    public JsonArray getDeliveringCartPvd(String token) {
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        List<Cart> carts = cartRepository.getDeliveringCartPvd();
        return getCart(carts);
    }
    private JsonArray getCart(List<Cart> carts){
        JsonArray r = new JsonArray();
        long customerId = 0;
        if (!carts.isEmpty()) {
            for (Cart cart : carts) {
                customerId = cart.getCustomerId();
                long totalPrice = 0;
                long discountPrice = 0;
                // ======= handle voucher under ============
                //TODO
                // ======= handle voucher above ============
                JsonObject js = new JsonObject();
                js.addProperty("cartId", cart.getId());
                js.addProperty("address", cart.getAddress());
                JsonArray booksCart = new JsonArray();
                JsonArray books = Logic.stringToJsonArray(cart.getBooks());
                JsonArray quantities = Logic.stringToJsonArray(cart.getQuantity());
                for (int i = 0; i < books.size(); i++) {
                    JsonObject book = new JsonObject();
                    Book bookEnt = bookService.getById(books.get(i).getAsLong());
                    book.addProperty("bookId", books.get(i).getAsLong());
                    book.addProperty("bookName", bookEnt.getName());
                    book.addProperty("bookAvatar", bookEnt.getImage());
                    book.addProperty("bookPrice", bookEnt.getPrice());
                    book.addProperty("quantity", quantities.get(i).getAsLong());
                    totalPrice += bookEnt.getPrice();
                    booksCart.add(book);
                }
                js.add("items", booksCart);
                js.addProperty("totalPrice", totalPrice);
                js.addProperty("discountPrice", discountPrice);
                js.addProperty("customer_id", customerId);
                r.add(js);
            }

            return r;
        } else {
            return new JsonArray();
        }
    }
}
