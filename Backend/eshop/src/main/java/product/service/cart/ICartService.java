package product.service.cart;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public interface ICartService {
    JsonObject updateBooksInCart(String token, JsonObject data);
    void updateCartInfo(String token, JsonObject data);
    void addBookToCart(String token, JsonObject data);
    void deleteFromCart(String token, long bookId);

    void payCart(String token);
    void cancelCart(String token,long cartId);


    JsonObject getMyCart(String token);

    JsonArray getWaitingCarts(String token);
    JsonArray getDeliveringCart(String token);
    JsonArray getCanceledCart(String token);


    JsonArray getReceivedCart(String token);


    JsonArray getWaitingCartPvd(String token);
    JsonArray getDeliveringCartPvd(String token);

    JsonArray getReceivedCartPvd(String token);

    JsonArray getCanceledCartPvd(String token);

    void acceptCart(String token, long cartId);

    void confirmCart(String token, long cartId);
}
