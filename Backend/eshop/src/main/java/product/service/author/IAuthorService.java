package product.service.author;

import product.dto.request.AuthorRequestDTO;
import product.entity.Author;

import java.util.List;

public interface IAuthorService {
    Author updateAuthor(long authorId, AuthorRequestDTO authorRequestDTO);
    List<Author> getAuthorList();
    Author addAuthor(AuthorRequestDTO authorRequestDTO);


}
