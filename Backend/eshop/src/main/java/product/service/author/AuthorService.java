package product.service.author;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import product.converter.AuthorConverter;
import product.dto.request.AuthorRequestDTO;
import product.entity.Author;
import product.repository.AuthorRepository;
import product.repository.UserRepository;
import product.security.JwtProvider;

import java.util.List;

@Service
public class AuthorService implements IAuthorService {
    @Autowired
    private JwtProvider jwtProvider;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuthorRepository authorRepository;
    public Author updateAuthor(long authorId, AuthorRequestDTO authorRequestDTO){
        Author author = AuthorConverter.toEntity(authorId, authorRequestDTO);
        author = authorRepository.save(author);
        return author;
    }
    public List<Author> getAuthorList(){
        List<Author> authors =  authorRepository.findAll();
        return authors;
    }

    public Author addAuthor(AuthorRequestDTO authorRequestDTO) {
        Author author = new Author();
        author.setYear(authorRequestDTO.getYear());
        author.setName(authorRequestDTO.getName());
        author.setNationality(authorRequestDTO.getNationality());
        author = authorRepository.save(author);
        return author;
    }
}
