package product.service.message;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import product.entity.Message;
import product.exception.EshopAPIException;
import product.repository.MessageRepository;
import product.repository.UserRepository;
import product.security.JwtProvider;
import product.util.GsonUtil;
import product.util.Logic;

import java.util.List;
import java.util.Optional;

@Component
public class MessageService implements IMessageService {
    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private JwtProvider jwtProvider;
    @Autowired
    private UserRepository userRepository;

    @Override
    public List<Message> getAllMessage(String token) {
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        List<Message> messages = messageRepository.getAllMessage(userId);
        return messages;
    }

    public void sendMessage(String token, JsonObject data) {
        String username = jwtProvider.getUsername(token);
        long from = userRepository.findByUsername(username).getId();
        long to = data.get("to").getAsLong();
        String cvId = Logic.toConversationId(from, to);
        String content = data.get("content").getAsString();

        Message message = new Message();
        message.setCvId(cvId);
        message.setTime(System.currentTimeMillis());
        message.setContent(content);
        message.setFrom(from);
        message.setTo(to);
        messageRepository.save(message);

    }

    @Override
    public JsonArray getConversation(String token, String conversationId) {
        String username = jwtProvider.getUsername(token);
        long userId = userRepository.findByUsername(username).getId();
        List<Message> msgs = messageRepository.findByCvId(conversationId);
        JsonArray ar = new JsonArray();
        for(Message m : msgs){
            JsonObject js = new JsonObject();
            js.addProperty("content", m.getContent());
            js.addProperty("from_user", m.getFrom());
            js.addProperty("to_user", m.getTo());
            js.addProperty("time", m.getTime());
            ar.add(js);
        }
        return ar;
    }
}
