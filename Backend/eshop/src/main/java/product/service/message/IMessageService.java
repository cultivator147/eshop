package product.service.message;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import product.entity.Message;

import java.util.List;

public interface IMessageService {
    List<Message> getAllMessage(String token);
    void sendMessage(String token, JsonObject data);

    JsonArray getConversation(String token, String conversationId);
}
