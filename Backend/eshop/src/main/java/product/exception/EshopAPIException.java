package product.exception;

import org.springframework.http.HttpStatus;

public class EshopAPIException extends RuntimeException{
    private HttpStatus status;
    private String message;

    public EshopAPIException(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public EshopAPIException(String message, HttpStatus status, String message1) {
        super(message);
        this.status = status;
        this.message = message1;
    }

    public HttpStatus getStatus() {
        return status;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
