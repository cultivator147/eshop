package product.exception.exception_test;

public class NotFoundException2 extends RuntimeException{
    public NotFoundException2(String message) {
        super(message);
    }
}
