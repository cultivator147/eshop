package product.exception.exception_test;

import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;


public class CustomExceptionHandler {
    @ExceptionHandler(NotFoundException2.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse handlerNotFoundException(NotFoundException2 ex, WebRequest re) {
        return new ErrorResponse(HttpStatus.NOT_FOUND, ex.getMessage());
    }
}
