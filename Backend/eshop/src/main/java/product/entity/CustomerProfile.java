package product.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name="customers")

public class CustomerProfile {
    @Id
    @Column(name="user_id")
    private long userid;
    @Column(name="name")

    private String name;
    @Column(name="avatar")

    private String avatar;
    @Column(name="gender")
    private String gender;
    @Column(name="address")

    private String address;
    @Column(name="birthday")

    private String birthday;
    @Column(name="interest")

    private String interest;
    @Column(name="phone_number")

    private String phoneNumber;
    @Column(name="email")

    private String email;

}
