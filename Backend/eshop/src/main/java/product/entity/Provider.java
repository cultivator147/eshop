package product.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name="providers")
public class Provider {
    @Id
    @Column(name="user_id")
    private long userId;
    @Column(name="name")

    private String name;
    @Column(name="avatar")

    private String avatar;

    @Column(name="address")

    private String address;

    @Column(name="phone_number")

    private String phoneNumber;
    @Column(name="email")

    private String email;
    @Column(name="followers")

    private long followers;
    @Column(name="tax")
    private String tax;
    @Column(name="detail_info")
    private String detailInfo;

}
