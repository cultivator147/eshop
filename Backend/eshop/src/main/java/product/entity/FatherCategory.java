package product.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "category_father")
public class FatherCategory {
    @Id
    @Column(name = "name")
    private String name;

    @Column(name = "categories_id")
    private String categories;
}
