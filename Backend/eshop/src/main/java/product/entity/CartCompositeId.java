package product.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable

public class CartCompositeId implements Serializable {
    @Column(name = "book_id")
    private long bookId;
    @Column(name = "customer_id")
    private long customerId;
    @Column(name = "status")
    private long status;
    @Column(name = "time")
    private long time;
}
