package product.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Embeddable
public class CommentCompositeKey implements Serializable {
    @Column(name = "book_id")
    private Long bookId;

    @Column(name = "user_id")
    private Long userId;
    @Column(name = "time")
    private Long time;
}
