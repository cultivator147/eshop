package product.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name="books_comments")
@IdClass(CommentCompositeKey.class)
public class Comment {
    @Id
    @Column(name = "book_id")
    @JoinTable(name = "books", joinColumns = @JoinColumn(name = "id", referencedColumnName = "book_id"))
    private Long bookId;
    @Id
    @Column(name = "user_id")
    @JoinTable(name = "users", joinColumns = @JoinColumn(name = "id", referencedColumnName = "user_id"))
    private Long userId;
    @Column(name = "comment")

    private String comment;
    @Id
    @Column(name = "time")
    private long time;
}
