package product.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "carts")
public class Cart {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private long id;
    @Column(name = "book_id")
    private String books;
    @Column(name = "customer_id")
    private long customerId;

    @Column(name = "quantity")

    private String quantity;

    @Column(name = "address")

    private String address;
    @Column(name = "voucher_id",  columnDefinition = "BIGINT DEFAULT 0")

    private long voucherId;
    @Column(name = "status")

    private int status;
    @Column(name = "phone_number")

    private String phoneNumber;
    @Column(name="user_info")
    private String userInfo;


}
