package product.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "messages")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private long id;
    @Column(name="cv_id")
    private String cvId;
    @Column(name = "from_user")
    private long from;
    @Column(name = "to_user")
    private long to;
    @Column(name="content")
    @Lob
    private String content;
    @Column(name="time")
    private long time;
}
