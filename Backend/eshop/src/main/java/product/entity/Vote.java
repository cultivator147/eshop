package product.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name="books_votes")
@IdClass(VoteCompositeKey.class)

public class Vote {
    @Id
    private long bookId;
    @Id
    private long userId;
    @Column(name = "vote_point")
    private int votePoint;
}
