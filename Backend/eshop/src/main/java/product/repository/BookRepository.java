package product.repository;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import product.entity.Book;
import product.entity.Comment;
import product.entity.Vote;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {
    @Modifying
    @Transactional
    @Query("SELECT b FROM Book b JOIN b.authors a WHERE b.name LIKE %:key% OR  a.name LIKE %:key% AND b.status = 1")
//    @Query(value = "SELECT DISTINCT b.* FROM books b, authors a, books_authors ba WHERE (b.name LIKE %:key%) OR (a.name LIKE %:key% AND a.id = ba.author_id AND ba.book_id = b.id);", nativeQuery = true)
    List<Book> searchBooks(@Param("key") String keySearch);

    @Query("SELECT b FROM Book b JOIN b.categories c WHERE c.id = :categoryId AND b.status = 1")
    List<Book> findByCategoryId(@Param("categoryId") long categoryId);

    @Query("SELECT cm FROM Comment cm JOIN Book b WHERE b.id = cm.bookId")
    List<Comment> getComment(long bookId);

    @Query("SELECT v FROM Vote v WHERE v.userId = :userId AND v.bookId = :bookId")
    Vote getVoted(long userId, long bookId);
    @Query(value = "SELECT v FROM Vote v WHERE v.bookId = :bookId")
    List<Vote> getVotes(long bookId);

    @Query("SELECT cm FROM Comment cm WHERE cm.userId = :userId")
    List<Comment> getMyComment(long userId);
    @Query("SELECT b FROM Book b JOIN b.publishingCompanies pc WHERE pc.id = :companyId AND pc.status = 1 AND b.status = 1 AND pc.status = 1")

    List<Book> findByCompanyId(Long companyId);

    @Query("SELECT b FROM Book b JOIN b.providers pv WHERE pv.userId = :providerId AND b.status = 1")
    List<Book> findByProviderId(Long providerId);
    @Transactional
    @Modifying
    @Query("UPDATE Book b SET b.status = 0 WHERE b.id = :bookId")
    void deleteBook(Long bookId);
    @Query("SELECT b FROM Book b JOIN b.authors a WHERE a.id = :authorId AND b.status = 1")
    List<Book> findByAuthorId(Long authorId);
}
