package product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import product.entity.Author;

import java.util.List;
import java.util.Optional;

public interface AuthorRepository  extends JpaRepository<Author, Long> {
}
