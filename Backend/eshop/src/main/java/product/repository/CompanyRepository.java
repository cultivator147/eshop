package product.repository;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import product.entity.PublishingCompany;

import java.util.List;

public interface CompanyRepository extends JpaRepository<PublishingCompany, Long> {
    @Transactional
    @Modifying
    @Query("UPDATE PublishingCompany c SET c.status = 0 WHERE c.id = :companyId")
    void deleteCompany(long companyId);
    @Query("SELECT  c FROM PublishingCompany  c WHERE c.status = 1")
    List<PublishingCompany> getAll();
}
