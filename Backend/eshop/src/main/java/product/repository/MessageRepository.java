package product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import product.entity.Message;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, String> {
    @Query("SELECT m FROM Message m WHERE m.from = :userId OR m.to = :userId")
    List<Message> getAllMessage(long userId);
    @Query("SELECT m FROM Message m WHERE m.cvId = :cvId")
    List<Message> findByCvId(String cvId);
}
