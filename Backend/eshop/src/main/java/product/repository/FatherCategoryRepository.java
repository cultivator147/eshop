package product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import product.entity.FatherCategory;


public interface FatherCategoryRepository extends JpaRepository<FatherCategory, String> {
}
