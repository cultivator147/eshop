package product.repository;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import product.entity.Role;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(String name);
    @Modifying
    @Transactional
    @Query(value = "INSERT INTO users_roles(user_id, role_id) VALUE(:userId, :roleId);", nativeQuery = true)
    void addUsersRole(long userId, long roleId);
}
