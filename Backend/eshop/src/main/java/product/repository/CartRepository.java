package product.repository;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import product.entity.Cart;

import java.util.List;
import java.util.Optional;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {
    @Query("SELECT c FROM Cart c WHERE c.customerId = :userId AND c.status = 0")
    Optional<Cart> getMyCart(long userId);

    @Query("SELECT c FROM Cart c WHERE c.customerId = :userId AND c.status = 1")
    List<Cart> getWaitingCarts(long userId);
    @Query("SELECT c FROM Cart c WHERE c.customerId = :userId AND c.status = 2")
    List<Cart> getDeliveringCart(long userId);
    @Query("SELECT c FROM Cart c WHERE c.customerId = :userId AND c.status = 3")

    List<Cart> getReceivedCart(long userId);

    @Transactional
    @Modifying
    @Query("UPDATE Cart c SET c.status = 1 WHERE c.customerId = :userId")
    void payCart(long userId);
    @Query("SELECT c FROM Cart c WHERE c.customerId = :userId AND c.status = -1")

    List<Cart> getCanceledCart(long userId);

    //BOTH 2
    @Transactional
    @Modifying
    @Query("UPDATE Cart c SET c.status = -1 WHERE c.id = :cartId")
    void cancelCart(long cartId);

    @Query("SELECT c FROM Cart c WHERE c.status = 1")
    List<Cart> getWaitingCartsPvd();
    @Query("SELECT c FROM Cart c WHERE c.status = 2")
    List<Cart> getDeliveringCartPvd();

    @Query("SELECT c FROM Cart c WHERE c.status = 3")

    List<Cart> getReceivedCartPvd();
    @Query("SELECT c FROM Cart c WHERE c.status = -1")

    List<Cart> getCanceledCartPvd();
    @Transactional
    @Modifying
    @Query("UPDATE Cart c SET c.status = 2 WHERE c.id = :cartId")
    void acceptCart(long cartId);
    @Transactional
    @Modifying
    @Query("UPDATE Cart  c SET c.status = 3 WHERE c.id = :cartId")
    void confirmCart(long cartId);
}
