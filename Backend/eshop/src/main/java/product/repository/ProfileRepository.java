package product.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import product.entity.CustomerProfile;

public interface ProfileRepository extends JpaRepository<CustomerProfile, Long> {

}
