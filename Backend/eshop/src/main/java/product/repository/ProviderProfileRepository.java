package product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import product.entity.Provider;

public interface ProviderProfileRepository extends JpaRepository<Provider,Long> {
}
