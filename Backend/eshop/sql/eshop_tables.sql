DROP TABLE IF EXISTS users;
CREATE TABLE users
(
    id           BIGINT PRIMARY KEY AUTO_INCREMENT,
    username     NVARCHAR(200) UNIQUE KEY,
    password     NVARCHAR(2048),
    token        NVARCHAR(2048),
    tok_exp_time   BIGINT,
    status       TINYINT
);

DROP TABLE IF EXISTS roles;
CREATE TABLE roles(
                      id BIGINT PRIMARY KEY AUTO_INCREMENT,
                      name NVARCHAR(200)
);


DROP TABLE IF EXISTS customers;
CREATE TABLE customers
(
    user_id      BIGINT primary key,
    name        NVARCHAR(500),
    avatar      NVARCHAR(2048),
    gender      NVARCHAR(50),
    address     NVARCHAR(500),
    birthday    NVARCHAR(50),
    interest    TEXT,
    phone_number NVARCHAR(100),
    email       NVARCHAR(200)
);

DROP TABLE IF EXISTS providers;
CREATE TABLE providers
(
    user_id      BIGINT PRIMARY KEY ,
    name        NVARCHAR(500),
    avatar      NVARCHAR(2048),
    address     NVARCHAR(500),
    phone_number NVARCHAR(50),
    email       NVARCHAR(200),
    followers   BIGINT DEFAULT 0,
    tax         NVARCHAR(2048),
    detail_info  TEXT
);

DROP TABLE IF EXISTS authors;
CREATE TABLE authors
(
    id             BIGINT PRIMARY KEY AUTO_INCREMENT,
    name           NVARCHAR(2048),
    year           INT,
    nationality    NVARCHAR(200)
);

DROP TABLE IF EXISTS publishing_companies;
CREATE TABLE publishing_companies
(
    id      BIGINT PRIMARY KEY AUTO_INCREMENT,
    name    NVARCHAR(500),
    address NVARCHAR(500),
    status INT DEFAULT 1
);


DROP TABLE IF EXISTS carts;
CREATE TABLE carts
(
    id BIGINT primary key AUTO_INCREMENT,
    book_id TEXT,
    customer_id BIGINT,
    supplier_id BIGINT,
    quantity BIGINT DEFAULT 1,
    time           DATETIME,
    status         tinyint,
    address         TEXT,
    voucher_id		BIGINT,
    phone_number VARCHAR(20),
    user_info TEXT
);

# 1:Đang nằm trong giỏ hàng, 2 : đã mua, đang giao hàng, 3: khách hàng đã nhận
	   #references to vouchers,là 1 json array information TEXT
	   #Số            lượng sách trong 1   customer_id BIGINT,
    supplier_id     BIGINT,
    book_id BIGINT,đơn hàng
	   #refer to books_template_id

DROP TABLE IF EXISTS categories;
CREATE TABLE categories
(
    id   int primary key auto_increment,
    name NVARCHAR(100) UNIQUE KEY
);
DROP TABLE IF EXISTS category_father;
CREATE TABLE category_father(
    name NVARCHAR(200) PRIMARY KEY,
    categories_id VARCHAR(2048)
);


DROP TABLE IF EXISTS books;
CREATE TABLE books
(
    id                  BIGINT PRIMARY KEY AUTO_INCREMENT,
    name                NVARCHAR(2048),
    image               NVARCHAR(2048),
    number_of_pages       INT,
    size                NVARCHAR(200),
    weight              INT,    #đơn vị gam
    price               BIGINT,
    year                INT,
    introduction        TEXT,
    status              INT DEFAULT 1
);

DROP TABLE IF EXISTS vouchers;
CREATE TABLE vouchers
(
    id       BIGINT primary key auto_increment,
    title    NVARCHAR(500),
    content  TEXT,
    time_from DATETIME,
    time_to   DATETIME,
    discount DECIMAL,
    type     INT,
    status   INT  #1: chưa dùng, 2 -đã dùng
);

DROP TABLE IF EXISTS books_comments;
CREATE TABLE books_comments(
    book_id BIGINT,
    user_id BIGINT,
    comment TEXT,
    time BIGINT
);
ALTER TABLE books_comments ADD FOREIGN KEY (book_id) REFERENCES books(id);
ALTER TABLE books_comments ADD FOREIGN KEY (user_id) REFERENCES users(id);

DROP TABLE IF EXISTS books_votes;
CREATE TABLE books_votes(
    book_id BIGINT,
    user_id BIGINT,
    vote_point TINYINT
);
ALTER TABLE books_votes ADD FOREIGN KEY (book_id) REFERENCES books(id);
ALTER TABLE books_votes ADD PRIMARY KEY (book_id, user_id);
ALTER TABLE books_votes ADD FOREIGN KEY (user_id) REFERENCES users(id);




DROP TABLE IF EXISTS users_roles;
CREATE TABLE users_roles(
                            user_id  BIGINT,
                            role_id BIGINT
);

DROP TABLE IF EXISTS books_categories;
CREATE TABLE books_categories(
                              book_id BIGINT,
                              category_id INT
);
ALTER TABLE books_categories ADD FOREIGN KEY (category_id) REFERENCES categories(id);
ALTER TABLE books_categories ADD FOREIGN KEY (book_id) REFERENCES books(id);
# book_author
DROP TABLE IF EXISTS books_authors;
CREATE TABLE books_authors(
    author_id BIGINT,
    book_id BIGINT
);
ALTER TABLE books_authors ADD FOREIGN KEY (author_id) REFERENCES authors(id);
ALTER TABLE books_authors ADD FOREIGN KEY (book_id) REFERENCES books(id);


# book_publishing_company
DROP TABLE IF EXISTS books_publishing_companies;
CREATE TABLE books_publishing_companies(
                              publishing_company_id BIGINT,
                              book_id BIGINT
);
ALTER TABLE books_publishing_companies ADD FOREIGN KEY (publishing_company_id) REFERENCES publishing_companies(id);
ALTER TABLE books_publishing_companies ADD FOREIGN KEY (book_id) REFERENCES books(id);

CREATE TABLE IF NOT EXISTS `books_providers` (
    `book_id` bigint(20) NOT NULL,
    `provider_id` bigint(20) NOT NULL,
    PRIMARY KEY (`book_id`,`provider_id`),
    KEY `provider_id` (`provider_id`),
    CONSTRAINT `books_providers_ibfk_1` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`user_id`),
    CONSTRAINT `books_providers_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;