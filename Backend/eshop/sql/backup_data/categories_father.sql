-- --------------------------------------------------------
-- Host:                         103.145.63.98
-- Server version:               10.5.20-MariaDB-log - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table hieutm.category_father
CREATE TABLE IF NOT EXISTS `category_father` (
  `name` varchar(200) NOT NULL,
  `categories_id` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Dumping data for table hieutm.category_father: ~9 rows (approximately)
/*!40000 ALTER TABLE `category_father` DISABLE KEYS */;
INSERT INTO `category_father` (`name`, `categories_id`) VALUES
	('Best Seller', '[1,3,5]'),
	('Fiction', '[2,4,6]'),
	('Game puzzles', '[1,7,10]'),
	('Gift card', '[4,8]'),
	('Kid', '[2,11]'),
	('New book', '[2,3,6]'),
	('Nonfiction', '[1,2,3]'),
	('Special offer', '[5,6,7]'),
	('Ya', '[7,8]');
/*!40000 ALTER TABLE `category_father` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
