-- --------------------------------------------------------
-- Host:                         103.145.63.98
-- Server version:               10.5.20-MariaDB-log - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table hieutm.books_publishing_companies
CREATE TABLE IF NOT EXISTS `books_publishing_companies` (
  `book_id` bigint(20) NOT NULL,
  `publishing_company_id` bigint(20) NOT NULL,
  PRIMARY KEY (`book_id`,`publishing_company_id`),
  KEY `publishing_company_id` (`publishing_company_id`),
  CONSTRAINT `books_publishing_companies_ibfk_1` FOREIGN KEY (`publishing_company_id`) REFERENCES `publishing_companies` (`id`),
  CONSTRAINT `books_publishing_companies_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Dumping data for table hieutm.books_publishing_companies: ~2,724 rows (approximately)
/*!40000 ALTER TABLE `books_publishing_companies` DISABLE KEYS */;
INSERT INTO `books_publishing_companies` (`book_id`, `publishing_company_id`) VALUES
	(1, 3),
	(2, 3),
	(3, 14),
	(4, 14),
	(5, 6),
	(6, 7),
	(7, 4),
	(8, 12),
	(9, 3),
	(10, 6),
	(11, 9),
	(12, 14),
	(13, 12),
	(14, 7),
	(15, 3),
	(16, 14),
	(17, 2),
	(18, 3),
	(19, 14),
	(20, 4),
	(21, 1),
	(22, 12),
	(23, 4),
	(52, 11),
	(100, 1),
	(101, 10),
	(102, 11),
	(152, 3),
	(153, 11),
	(202, 9),
	(1000, 1),
	(1001, 3),
	(1002, 7),
	(1003, 12),
	(1004, 14),
	(1005, 9),
	(1006, 6),
	(1007, 13),
	(1008, 12),
	(1009, 11),
	(1010, 6),
	(1011, 14),
	(1012, 14),
	(1013, 8),
	(1014, 12),
	(1015, 9),
	(1016, 11),
	(1017, 7),
	(1018, 1),
	(1019, 12),
	(1020, 1),
	(1021, 1),
	(1022, 1),
	(1023, 1),
	(1024, 12),
	(1025, 9),
	(1026, 15),
	(1027, 8),
	(1028, 3),
	(1029, 13),
	(1030, 6),
	(1031, 12),
	(1032, 7),
	(1033, 1),
	(1034, 14),
	(1035, 2),
	(1036, 4),
	(1037, 14),
	(1038, 2),
	(1039, 9),
	(1040, 8),
	(1041, 2),
	(1042, 11),
	(1043, 6),
	(1044, 3),
	(1045, 4),
	(1046, 15),
	(1047, 13),
	(1048, 2),
	(1049, 9),
	(1050, 1),
	(1051, 12),
	(1052, 3),
	(1053, 12),
	(1054, 14),
	(1055, 8),
	(1056, 12),
	(1057, 10),
	(1058, 11),
	(1059, 2),
	(1060, 1),
	(1061, 14),
	(1062, 3),
	(1063, 12),
	(1064, 9),
	(1065, 7),
	(1066, 10),
	(1067, 7),
	(1068, 11),
	(1069, 3),
	(1070, 7),
	(1071, 12),
	(1072, 11),
	(1073, 6),
	(1074, 4),
	(1075, 11),
	(1076, 1),
	(1077, 2),
	(1078, 8),
	(1079, 13),
	(1080, 8),
	(1081, 2),
	(1082, 4),
	(1083, 14),
	(1084, 11),
	(1085, 12),
	(1086, 11),
	(1087, 14),
	(1088, 4),
	(1089, 10),
	(1090, 3),
	(1091, 7),
	(1092, 2),
	(1093, 10),
	(1094, 1),
	(1095, 6),
	(1096, 4),
	(1097, 15),
	(1098, 9),
	(1099, 8),
	(1100, 11),
	(1101, 10),
	(1102, 1),
	(1103, 1),
	(1104, 6),
	(1105, 1),
	(1106, 9),
	(1107, 8),
	(1108, 5),
	(1109, 5),
	(1110, 11),
	(1111, 2),
	(1112, 7),
	(1113, 6),
	(1114, 5),
	(1115, 11),
	(1116, 10),
	(1117, 3),
	(1118, 6),
	(1119, 1),
	(1120, 5),
	(1121, 4),
	(1122, 2),
	(1123, 6),
	(1124, 12),
	(1125, 11),
	(1126, 12),
	(1127, 11),
	(1128, 5),
	(1129, 6),
	(1130, 11),
	(1131, 3),
	(1132, 7),
	(1133, 5),
	(1134, 14),
	(1135, 1),
	(1136, 8),
	(1137, 10),
	(1138, 2),
	(1139, 5),
	(1140, 10),
	(1141, 4),
	(1142, 2),
	(1143, 4),
	(2000, 4),
	(2001, 15),
	(2002, 13),
	(2003, 9),
	(2004, 6),
	(2005, 4),
	(2006, 6),
	(2007, 11),
	(2008, 10),
	(2009, 11),
	(2010, 4),
	(2011, 1),
	(2012, 4),
	(2013, 12),
	(2014, 15),
	(2015, 5),
	(2016, 10),
	(2017, 11),
	(2018, 9),
	(2019, 9),
	(2020, 9),
	(2021, 9),
	(2022, 15),
	(2023, 7),
	(2024, 11),
	(2025, 12),
	(2026, 3),
	(2027, 7),
	(2028, 7),
	(2029, 3),
	(2030, 3),
	(2031, 5),
	(2032, 5),
	(2033, 10),
	(2034, 12),
	(2035, 10),
	(2036, 12),
	(2037, 6),
	(2038, 7),
	(2039, 15),
	(2040, 2),
	(2041, 11),
	(2042, 10),
	(2043, 3),
	(2044, 11),
	(2045, 2),
	(2046, 12),
	(2047, 2),
	(2048, 14),
	(2049, 5),
	(2050, 12),
	(2051, 2),
	(2052, 10),
	(2053, 8),
	(2054, 8),
	(2055, 5),
	(2056, 12),
	(2057, 1),
	(2058, 9),
	(2059, 7),
	(2060, 14),
	(2061, 11),
	(2062, 7),
	(2063, 11),
	(2064, 15),
	(2065, 14),
	(2066, 2),
	(2067, 3),
	(2068, 3),
	(2069, 6),
	(2070, 3),
	(2071, 14),
	(2072, 2),
	(2073, 7),
	(2074, 4),
	(2075, 3),
	(2076, 11),
	(2077, 3),
	(2078, 14),
	(2079, 5),
	(2080, 13),
	(2081, 4),
	(2082, 10),
	(2083, 8),
	(2084, 12),
	(2085, 14),
	(2086, 12),
	(2087, 10),
	(2088, 3),
	(2089, 1),
	(2090, 5),
	(2091, 7),
	(2092, 11),
	(2093, 7),
	(2094, 6),
	(2095, 5),
	(2096, 3),
	(2097, 4),
	(2098, 6),
	(2099, 13),
	(2100, 2),
	(2101, 4),
	(2102, 9),
	(2103, 3),
	(2104, 3),
	(2105, 7),
	(2106, 15),
	(2107, 11),
	(2108, 2),
	(2109, 9),
	(2110, 6),
	(2111, 9),
	(2112, 3),
	(2113, 6),
	(2114, 4),
	(2115, 10),
	(2116, 11),
	(2117, 7),
	(2118, 14),
	(2119, 5),
	(3000, 4),
	(3001, 13),
	(3002, 15),
	(3003, 15),
	(3004, 3),
	(3005, 10),
	(3006, 9),
	(3007, 11),
	(3008, 11),
	(3009, 2),
	(3010, 11),
	(3011, 11),
	(3012, 6),
	(3013, 15),
	(3014, 11),
	(3015, 3),
	(3016, 11),
	(3017, 4),
	(3018, 6),
	(3019, 4),
	(3020, 1),
	(3021, 2),
	(3022, 11),
	(3023, 3),
	(3024, 5),
	(3025, 13),
	(3026, 12),
	(3027, 3),
	(3028, 12),
	(3029, 3),
	(3030, 5),
	(3031, 14),
	(3032, 14),
	(3033, 15),
	(3034, 5),
	(3035, 8),
	(3036, 14),
	(3037, 5),
	(3038, 5),
	(3039, 2),
	(3040, 13),
	(3041, 1),
	(3042, 9),
	(3043, 12),
	(3044, 13),
	(3045, 14),
	(3046, 10),
	(3047, 2),
	(3048, 8),
	(3049, 14),
	(3050, 5),
	(3051, 5),
	(3052, 6),
	(3053, 12),
	(3054, 8),
	(3055, 11),
	(3056, 9),
	(3057, 13),
	(3058, 7),
	(3059, 14),
	(3060, 14),
	(3061, 8),
	(3062, 11),
	(3063, 14),
	(3064, 13),
	(3065, 1),
	(3066, 5),
	(3067, 15),
	(3068, 3),
	(3069, 1),
	(3070, 10),
	(3071, 15),
	(3072, 13),
	(3073, 14),
	(3074, 12),
	(3075, 15),
	(3076, 7),
	(3077, 3),
	(3078, 11),
	(3079, 4),
	(3080, 5),
	(3081, 9),
	(3082, 2),
	(3083, 11),
	(3084, 1),
	(3085, 1),
	(3086, 12),
	(3087, 14),
	(3088, 5),
	(3089, 2),
	(3090, 13),
	(3091, 9),
	(3092, 15),
	(3093, 3),
	(3094, 5),
	(3095, 8),
	(3096, 1),
	(3097, 13),
	(3098, 6),
	(3099, 5),
	(3100, 3),
	(3101, 4),
	(3102, 2),
	(3103, 9),
	(3104, 6),
	(3105, 7),
	(3106, 12),
	(3107, 2),
	(3108, 5),
	(3109, 6),
	(3110, 13),
	(3111, 9),
	(3112, 7),
	(3113, 5),
	(3114, 1),
	(3115, 14),
	(3116, 15),
	(3117, 15),
	(3118, 14),
	(3119, 9),
	(4000, 10),
	(4001, 12),
	(4002, 13),
	(4003, 11),
	(4004, 9),
	(4005, 15),
	(4006, 3),
	(4007, 13),
	(4008, 3),
	(4009, 5),
	(4010, 9),
	(4011, 6),
	(4012, 9),
	(4013, 13),
	(4014, 4),
	(4015, 7),
	(4016, 4),
	(4017, 14),
	(4018, 14),
	(4019, 13),
	(4020, 14),
	(4021, 1),
	(4022, 13),
	(4023, 5),
	(4024, 15),
	(4025, 13),
	(4026, 6),
	(4027, 1),
	(4028, 7),
	(4029, 5),
	(4030, 8),
	(4031, 5),
	(4032, 10),
	(4033, 12),
	(4034, 5),
	(4035, 6),
	(4036, 12),
	(4037, 9),
	(4038, 8),
	(4039, 5),
	(4040, 7),
	(4041, 12),
	(4042, 2),
	(4043, 4),
	(4044, 11),
	(4045, 1),
	(4046, 10),
	(4047, 9),
	(4048, 7),
	(4049, 4),
	(4050, 5),
	(4051, 15),
	(4052, 14),
	(4053, 5),
	(4054, 1),
	(4055, 4),
	(4056, 6),
	(4057, 4),
	(4058, 13),
	(4059, 8),
	(4060, 7),
	(4061, 2),
	(4062, 1),
	(4063, 8),
	(4064, 8),
	(4065, 11),
	(4066, 3),
	(4067, 12),
	(4068, 6),
	(4069, 2),
	(4070, 15),
	(4071, 3),
	(4072, 8),
	(4073, 13),
	(4074, 7),
	(4075, 15),
	(4076, 11),
	(4077, 8),
	(4078, 4),
	(4079, 10),
	(4080, 7),
	(4081, 8),
	(4082, 1),
	(4083, 3),
	(4084, 10),
	(4085, 7),
	(4086, 3),
	(4087, 4),
	(4088, 5),
	(4089, 13),
	(4090, 14),
	(4091, 1),
	(4092, 12),
	(4093, 13),
	(4094, 1),
	(4095, 13),
	(4096, 15),
	(4097, 8),
	(4098, 14),
	(4099, 10),
	(4100, 13),
	(4101, 12),
	(4102, 11),
	(4103, 5),
	(4104, 11),
	(4105, 5),
	(4106, 10),
	(4107, 13),
	(4108, 13),
	(4109, 2),
	(4110, 5),
	(4111, 3),
	(4112, 2),
	(4113, 13),
	(4114, 12),
	(4115, 11),
	(4116, 9),
	(4117, 1),
	(4118, 7),
	(4119, 10),
	(4120, 2),
	(4121, 9),
	(4122, 1),
	(4123, 4),
	(4124, 9),
	(4125, 8),
	(4126, 4),
	(4127, 14),
	(4128, 7),
	(4129, 10),
	(4130, 4),
	(4131, 4),
	(4132, 1),
	(4133, 6),
	(4134, 3),
	(4135, 3),
	(4136, 3),
	(4137, 11),
	(4138, 5),
	(4139, 7),
	(4140, 4),
	(4141, 3),
	(4142, 6),
	(4143, 7),
	(4144, 6),
	(4145, 9),
	(4146, 4),
	(4147, 12),
	(4148, 3),
	(4149, 1),
	(4150, 9),
	(4151, 6),
	(4152, 9),
	(4153, 4),
	(4154, 8),
	(4155, 11),
	(4156, 12),
	(4157, 14),
	(4158, 12),
	(4159, 12),
	(4160, 14),
	(4161, 13),
	(4162, 3),
	(4163, 2),
	(4164, 8),
	(4165, 11),
	(4166, 13),
	(4167, 7),
	(4168, 2),
	(4169, 8),
	(4170, 2),
	(4171, 11),
	(4172, 8),
	(4173, 10),
	(4174, 9),
	(4175, 4),
	(4176, 9),
	(4177, 14),
	(4178, 1),
	(4179, 12),
	(4180, 4),
	(4181, 5),
	(4182, 15),
	(4183, 7),
	(4184, 5),
	(4185, 11),
	(4186, 1),
	(4187, 5),
	(4188, 9),
	(4189, 14),
	(4190, 15),
	(4191, 11),
	(4192, 6),
	(4193, 1),
	(4194, 11),
	(4195, 15),
	(4196, 11),
	(4197, 9),
	(4198, 3),
	(4199, 5),
	(4200, 10),
	(4201, 14),
	(4202, 11),
	(4203, 9),
	(4204, 10),
	(4205, 2),
	(4206, 10),
	(4207, 13),
	(4208, 8),
	(4209, 12),
	(4210, 11),
	(4211, 1),
	(4212, 1),
	(4213, 8),
	(4214, 1),
	(4215, 6),
	(4216, 2),
	(4217, 12),
	(4218, 14),
	(4219, 11),
	(4220, 13),
	(4221, 6),
	(4222, 11),
	(4223, 14),
	(4224, 3),
	(4225, 12),
	(4226, 15),
	(4227, 10),
	(4228, 12),
	(4229, 7),
	(4230, 3),
	(4231, 4),
	(4232, 6),
	(4233, 9),
	(4234, 11),
	(4235, 12),
	(4236, 14),
	(4237, 10),
	(4238, 5),
	(4239, 5),
	(4240, 10),
	(4241, 3),
	(4242, 15),
	(4243, 5),
	(4244, 6),
	(4245, 9),
	(4246, 7),
	(4247, 12),
	(4248, 13),
	(4249, 9),
	(4250, 11),
	(4251, 1),
	(4252, 3),
	(4253, 13),
	(4254, 5),
	(4255, 1),
	(4256, 9),
	(4257, 8),
	(4258, 10),
	(4259, 6),
	(4260, 2),
	(4261, 8),
	(4262, 10),
	(4263, 11),
	(4264, 15),
	(4265, 7),
	(4266, 1),
	(4267, 9),
	(4268, 9),
	(4269, 13),
	(4270, 10),
	(4271, 8),
	(4272, 3),
	(4273, 8),
	(4274, 9),
	(4275, 11),
	(4276, 13),
	(4277, 3),
	(4278, 10),
	(4279, 10),
	(4280, 8),
	(4281, 8),
	(4282, 4),
	(4283, 14),
	(4284, 8),
	(4285, 5),
	(4286, 15),
	(4287, 7),
	(4288, 8),
	(4289, 5),
	(4290, 14),
	(4291, 12),
	(4292, 13),
	(4293, 3),
	(4294, 5),
	(4295, 6),
	(4296, 5),
	(4297, 8),
	(4298, 4),
	(4299, 10),
	(4300, 3),
	(4301, 10),
	(4302, 14),
	(4303, 5),
	(4304, 7),
	(4305, 11),
	(4306, 11),
	(4307, 13),
	(4308, 7),
	(4309, 2),
	(4310, 8),
	(4311, 12),
	(4312, 4),
	(4313, 9),
	(4314, 6),
	(4315, 11),
	(4316, 2),
	(4317, 1),
	(4318, 11),
	(4319, 12),
	(4320, 10),
	(4321, 11),
	(4322, 8),
	(4323, 14),
	(4324, 8),
	(4325, 13),
	(4326, 9),
	(4327, 4),
	(4328, 4),
	(4329, 8),
	(4330, 4),
	(4331, 7),
	(4332, 6),
	(4333, 11),
	(4334, 5),
	(4335, 14),
	(5000, 12),
	(5001, 6),
	(5002, 3),
	(5003, 8),
	(5004, 1),
	(5005, 10),
	(5006, 11),
	(5007, 5),
	(5008, 3),
	(5009, 3),
	(5010, 13),
	(5011, 4),
	(5012, 1),
	(5013, 6),
	(5014, 9),
	(5015, 12),
	(5016, 5),
	(5017, 13),
	(5018, 3),
	(5019, 7),
	(5020, 4),
	(5021, 3),
	(5022, 12),
	(5023, 15),
	(5024, 11),
	(5025, 14),
	(5026, 10),
	(5027, 5),
	(5028, 1),
	(5029, 10),
	(5030, 7),
	(5031, 6),
	(5032, 10),
	(5033, 11),
	(5034, 2),
	(5035, 7),
	(5036, 6),
	(5037, 1),
	(5038, 13),
	(5039, 2),
	(5040, 15),
	(5041, 5),
	(5042, 15),
	(5043, 14),
	(5044, 11),
	(5045, 9),
	(5046, 12),
	(5047, 5),
	(5048, 14),
	(5049, 8),
	(5050, 8),
	(5051, 9),
	(5052, 10),
	(5053, 9),
	(5054, 10),
	(5055, 3),
	(5056, 7),
	(5057, 1),
	(5058, 2),
	(5059, 11),
	(5060, 7),
	(5061, 14),
	(5062, 15),
	(5063, 3),
	(5064, 1),
	(5065, 13),
	(5066, 7),
	(5067, 13),
	(5068, 5),
	(5069, 3),
	(5070, 6),
	(5071, 11),
	(5072, 3),
	(5073, 1),
	(5074, 7),
	(5075, 6),
	(5076, 9),
	(5077, 6),
	(5078, 3),
	(5079, 7),
	(5080, 12),
	(5081, 6),
	(5082, 2),
	(5083, 9),
	(5084, 8),
	(5085, 4),
	(5086, 12),
	(5087, 11),
	(5088, 2),
	(5089, 5),
	(5090, 8),
	(5091, 13),
	(5092, 1),
	(5093, 5),
	(5094, 2),
	(5095, 2),
	(5096, 3),
	(5097, 15),
	(5098, 2),
	(5099, 9),
	(5100, 1),
	(5101, 9),
	(5102, 8),
	(5103, 3),
	(5104, 4),
	(5105, 7),
	(5106, 9),
	(5107, 5),
	(5108, 2),
	(5109, 11),
	(5110, 8),
	(5111, 7),
	(5112, 13),
	(5113, 4),
	(5114, 10),
	(5115, 2),
	(5116, 13),
	(5117, 13),
	(5118, 6),
	(5119, 10),
	(5120, 3),
	(5121, 2),
	(5122, 2),
	(5123, 7),
	(5124, 5),
	(5125, 11),
	(5126, 5),
	(5127, 2),
	(5128, 8),
	(5129, 4),
	(5130, 8),
	(5131, 14),
	(5132, 8),
	(5133, 3),
	(5134, 15),
	(5135, 4),
	(5136, 8),
	(5137, 4),
	(5138, 15),
	(5139, 12),
	(5140, 15),
	(5141, 13),
	(5142, 3),
	(5143, 14),
	(5144, 1),
	(5145, 10),
	(5146, 6),
	(5147, 13),
	(5148, 2),
	(5149, 3),
	(5150, 9),
	(5151, 14),
	(5152, 5),
	(5153, 3),
	(5154, 13),
	(5155, 9),
	(5156, 7),
	(5157, 15),
	(5158, 5),
	(5159, 13),
	(5160, 15),
	(5161, 3),
	(5162, 12),
	(5163, 9),
	(5164, 2),
	(5165, 1),
	(5166, 1),
	(5167, 11),
	(5168, 2),
	(5169, 12),
	(5170, 13),
	(5171, 6),
	(5172, 7),
	(5173, 6),
	(5174, 3),
	(5175, 13),
	(5176, 14),
	(5177, 9),
	(5178, 10),
	(5179, 15),
	(5180, 6),
	(5181, 7),
	(5182, 14),
	(5183, 7),
	(5184, 1),
	(5185, 1),
	(5186, 10),
	(5187, 1),
	(5188, 1),
	(5189, 11),
	(5190, 15),
	(5191, 2),
	(5192, 8),
	(5193, 10),
	(5194, 5),
	(5195, 12),
	(5196, 9),
	(5197, 3),
	(5198, 11),
	(5199, 15),
	(5200, 9),
	(5201, 15),
	(5202, 3),
	(5203, 15),
	(5204, 14),
	(5205, 14),
	(5206, 14),
	(5207, 6),
	(5208, 8),
	(5209, 8),
	(5210, 10),
	(5211, 1),
	(5212, 4),
	(5213, 2),
	(5214, 3),
	(5215, 1),
	(5216, 8),
	(5217, 15),
	(5218, 12),
	(5219, 2),
	(5220, 11),
	(5221, 10),
	(5222, 6),
	(5223, 4),
	(5224, 13),
	(5225, 10),
	(5226, 10),
	(5227, 15),
	(5228, 10),
	(5229, 15),
	(5230, 9),
	(5231, 1),
	(5232, 2),
	(5233, 4),
	(5234, 8),
	(5235, 15),
	(5236, 8),
	(5237, 3),
	(5238, 5),
	(5239, 15),
	(5240, 5),
	(5241, 15),
	(5242, 6),
	(5243, 4),
	(5244, 13),
	(5245, 3),
	(5246, 4),
	(5247, 12),
	(5248, 8),
	(5249, 9),
	(5250, 12),
	(5251, 8),
	(5252, 14),
	(5253, 8),
	(5254, 7),
	(5255, 12),
	(5256, 10),
	(5257, 12),
	(5258, 2),
	(5259, 4),
	(5260, 2),
	(5261, 9),
	(5262, 13),
	(5263, 1),
	(5264, 2),
	(5265, 11),
	(5266, 9),
	(5267, 7),
	(5268, 15),
	(5269, 2),
	(5270, 4),
	(5271, 2),
	(5272, 7),
	(5273, 11),
	(5274, 8),
	(5275, 7),
	(5276, 15),
	(5277, 8),
	(5278, 13),
	(5279, 6),
	(5280, 1),
	(5281, 2),
	(5282, 5),
	(5283, 11),
	(5284, 13),
	(5285, 2),
	(5286, 8),
	(5287, 8),
	(5288, 12),
	(5289, 14),
	(5290, 13),
	(5291, 3),
	(5292, 2),
	(5293, 10),
	(5294, 3),
	(5295, 13),
	(5296, 6),
	(5297, 7),
	(5298, 3),
	(5299, 13),
	(5300, 2),
	(5301, 3),
	(5302, 12),
	(5303, 12),
	(5304, 1),
	(5305, 1),
	(5306, 5),
	(5307, 12),
	(5308, 9),
	(5309, 2),
	(5310, 15),
	(5311, 9),
	(6000, 8),
	(6001, 3),
	(6002, 2),
	(6003, 6),
	(6004, 12),
	(6005, 3),
	(6006, 1),
	(6007, 3),
	(6008, 1),
	(6009, 10),
	(6010, 3),
	(6011, 15),
	(6012, 12),
	(6013, 1),
	(6014, 4),
	(6015, 15),
	(6016, 15),
	(6017, 7),
	(6018, 4),
	(6019, 3),
	(6020, 2),
	(6021, 13),
	(6022, 13),
	(6023, 1),
	(6024, 5),
	(6025, 7),
	(6026, 5),
	(6027, 7),
	(6028, 11),
	(6029, 15),
	(6030, 14),
	(6031, 11),
	(6032, 10),
	(6033, 10),
	(6034, 5),
	(6035, 9),
	(6036, 3),
	(6037, 6),
	(6038, 13),
	(6039, 2),
	(6040, 1),
	(6041, 13),
	(6042, 12),
	(6043, 8),
	(6044, 7),
	(6045, 5),
	(6046, 5),
	(6047, 12),
	(6048, 13),
	(6049, 15),
	(6050, 9),
	(6051, 11),
	(6052, 9),
	(6053, 7),
	(6054, 2),
	(6055, 1),
	(6056, 1),
	(6057, 10),
	(6058, 7),
	(6059, 5),
	(6060, 12),
	(6061, 5),
	(6062, 1),
	(6063, 14),
	(6064, 7),
	(6065, 10),
	(6066, 1),
	(6067, 5),
	(6068, 4),
	(6069, 9),
	(6070, 2),
	(6071, 12),
	(6072, 1),
	(6073, 13),
	(6074, 10),
	(6075, 2),
	(6076, 7),
	(6077, 5),
	(6078, 4),
	(6079, 15),
	(6080, 13),
	(6081, 3),
	(6082, 10),
	(6083, 7),
	(6084, 11),
	(6085, 3),
	(6086, 13),
	(6087, 7),
	(6088, 5),
	(6089, 1),
	(6090, 2),
	(6091, 13),
	(6092, 14),
	(6093, 5),
	(6094, 11),
	(6095, 10),
	(6096, 12),
	(6097, 14),
	(6098, 7),
	(6099, 7),
	(6100, 3),
	(6101, 13),
	(6102, 1),
	(6103, 12),
	(6104, 11),
	(6105, 10),
	(6106, 8),
	(6107, 2),
	(6108, 5),
	(6109, 14),
	(6110, 13),
	(6111, 3),
	(6112, 11),
	(6113, 11),
	(6114, 3),
	(6115, 11),
	(6116, 7),
	(6117, 14),
	(6118, 9),
	(6119, 14),
	(6120, 8),
	(6121, 15),
	(6122, 10),
	(6123, 3),
	(6124, 4),
	(6125, 2),
	(6126, 1),
	(6127, 11),
	(6128, 4),
	(6129, 8),
	(6130, 4),
	(6131, 8),
	(6132, 10),
	(6133, 10),
	(6134, 12),
	(6135, 2),
	(6136, 12),
	(6137, 2),
	(6138, 4),
	(6139, 6),
	(6140, 13),
	(6141, 14),
	(6142, 9),
	(6143, 2),
	(6144, 1),
	(6145, 6),
	(6146, 5),
	(6147, 5),
	(6148, 14),
	(6149, 15),
	(6150, 12),
	(6151, 15),
	(6152, 3),
	(6153, 12),
	(6154, 13),
	(6155, 10),
	(6156, 1),
	(6157, 9),
	(6158, 6),
	(6159, 15),
	(6160, 13),
	(6161, 14),
	(6162, 9),
	(6163, 8),
	(6164, 13),
	(6165, 4),
	(6166, 1),
	(6167, 9),
	(6168, 6),
	(6169, 14),
	(6170, 8),
	(6171, 1),
	(6172, 10),
	(6173, 8),
	(6174, 12),
	(6175, 13),
	(6176, 13),
	(6177, 13),
	(6178, 4),
	(6179, 4),
	(6180, 6),
	(6181, 2),
	(6182, 1),
	(6183, 5),
	(6184, 6),
	(6185, 11),
	(6186, 9),
	(6187, 4),
	(6188, 13),
	(6189, 7),
	(6190, 1),
	(6191, 15),
	(6192, 9),
	(6193, 14),
	(6194, 3),
	(6195, 1),
	(6196, 14),
	(6197, 4),
	(6198, 9),
	(6199, 4),
	(6200, 1),
	(6201, 2),
	(6202, 2),
	(6203, 15),
	(6204, 12),
	(6205, 11),
	(6206, 15),
	(6207, 7),
	(6208, 15),
	(6209, 7),
	(6210, 13),
	(6211, 2),
	(6212, 11),
	(6213, 5),
	(6214, 4),
	(6215, 4),
	(6216, 7),
	(6217, 14),
	(6218, 8),
	(6219, 8),
	(6220, 8),
	(6221, 4),
	(6222, 1),
	(6223, 3),
	(6224, 7),
	(6225, 7),
	(6226, 2),
	(6227, 1),
	(6228, 9),
	(6229, 2),
	(6230, 15),
	(6231, 3),
	(6232, 7),
	(6233, 4),
	(6234, 14),
	(6235, 8),
	(6236, 11),
	(6237, 6),
	(6238, 5),
	(6239, 7),
	(6240, 12),
	(6241, 6),
	(6242, 11),
	(6243, 4),
	(6244, 7),
	(6245, 2),
	(6246, 7),
	(6247, 5),
	(6248, 15),
	(6249, 12),
	(6250, 2),
	(6251, 2),
	(6252, 11),
	(6253, 4),
	(6254, 15),
	(6255, 12),
	(6256, 10),
	(6257, 6),
	(6258, 11),
	(6259, 14),
	(6260, 8),
	(6261, 2),
	(6262, 14),
	(6263, 4),
	(6264, 5),
	(6265, 7),
	(6266, 1),
	(6267, 14),
	(6268, 5),
	(6269, 9),
	(6270, 1),
	(6271, 11),
	(6272, 3),
	(6273, 1),
	(6274, 7),
	(6275, 11),
	(6276, 13),
	(6277, 14),
	(6278, 4),
	(6279, 13),
	(6280, 11),
	(6281, 10),
	(6282, 4),
	(6283, 7),
	(6284, 6),
	(6285, 7),
	(6286, 6),
	(6287, 9),
	(6288, 5),
	(6289, 5),
	(6290, 10),
	(6291, 1),
	(6292, 12),
	(6293, 2),
	(6294, 11),
	(6295, 8),
	(6296, 2),
	(6297, 9),
	(6298, 2),
	(6299, 12),
	(6300, 7),
	(6301, 5),
	(6302, 6),
	(6303, 12),
	(6304, 15),
	(6305, 6),
	(6306, 5),
	(6307, 14),
	(6308, 8),
	(6309, 8),
	(6310, 14),
	(6311, 11),
	(7000, 6),
	(7001, 5),
	(7002, 7),
	(7003, 1),
	(7004, 5),
	(7005, 14),
	(7006, 5),
	(7007, 4),
	(7008, 9),
	(7009, 10),
	(7010, 6),
	(7011, 14),
	(7012, 2),
	(7013, 3),
	(7014, 13),
	(7015, 3),
	(7016, 5),
	(7017, 13),
	(7018, 7),
	(7019, 9),
	(7020, 8),
	(7021, 10),
	(7022, 14),
	(7023, 9),
	(7024, 9),
	(7025, 14),
	(7026, 4),
	(7027, 15),
	(7028, 8),
	(7029, 12),
	(7030, 15),
	(7031, 15),
	(7032, 12),
	(7033, 8),
	(7034, 4),
	(7035, 2),
	(7036, 6),
	(7037, 1),
	(7038, 6),
	(7039, 10),
	(7040, 11),
	(7041, 12),
	(7042, 5),
	(7043, 3),
	(7044, 1),
	(7045, 12),
	(7046, 7),
	(7047, 10),
	(7048, 6),
	(7049, 12),
	(7050, 2),
	(7051, 2),
	(7052, 12),
	(7053, 1),
	(7054, 14),
	(7055, 9),
	(7056, 6),
	(7057, 14),
	(7058, 15),
	(7059, 6),
	(7060, 8),
	(7061, 1),
	(7062, 7),
	(7063, 4),
	(7064, 6),
	(7065, 5),
	(7066, 11),
	(7067, 15),
	(7068, 15),
	(7069, 8),
	(7070, 10),
	(7071, 5),
	(7072, 13),
	(7073, 1),
	(7074, 14),
	(7075, 11),
	(7076, 15),
	(7077, 6),
	(7078, 3),
	(7079, 14),
	(7080, 7),
	(7081, 5),
	(7082, 12),
	(7083, 7),
	(7084, 2),
	(7085, 6),
	(7086, 1),
	(7087, 4),
	(7088, 5),
	(7089, 2),
	(7090, 10),
	(7091, 15),
	(7092, 10),
	(7093, 13),
	(7094, 14),
	(7095, 7),
	(7096, 11),
	(7097, 15),
	(7098, 8),
	(7099, 14),
	(7100, 9),
	(7101, 7),
	(7102, 7),
	(7103, 11),
	(7104, 11),
	(7105, 9),
	(7106, 3),
	(7107, 5),
	(7108, 14),
	(7109, 1),
	(7110, 3),
	(7111, 15),
	(7112, 14),
	(7113, 1),
	(7114, 15),
	(7115, 6),
	(7116, 1),
	(7117, 6),
	(7118, 11),
	(7119, 7),
	(7120, 10),
	(7121, 10),
	(7122, 12),
	(7123, 13),
	(7124, 11),
	(7125, 3),
	(7126, 15),
	(7127, 5),
	(7128, 12),
	(7129, 11),
	(7130, 8),
	(7131, 7),
	(7132, 5),
	(7133, 12),
	(7134, 1),
	(7135, 1),
	(7136, 9),
	(7137, 15),
	(7138, 6),
	(7139, 10),
	(7140, 7),
	(7141, 4),
	(7142, 4),
	(7143, 10),
	(7144, 8),
	(7145, 7),
	(7146, 8),
	(7147, 6),
	(7148, 12),
	(7149, 15),
	(7150, 15),
	(7151, 8),
	(7152, 13),
	(7153, 13),
	(7154, 13),
	(7155, 12),
	(7156, 11),
	(7157, 10),
	(7158, 14),
	(7159, 11),
	(7160, 8),
	(7161, 14),
	(7162, 15),
	(7163, 11),
	(7164, 1),
	(7165, 15),
	(7166, 12),
	(7167, 10),
	(7168, 5),
	(7169, 2),
	(7170, 10),
	(7171, 3),
	(7172, 15),
	(7173, 9),
	(7174, 8),
	(7175, 14),
	(7176, 12),
	(7177, 7),
	(7178, 3),
	(7179, 14),
	(7180, 4),
	(7181, 13),
	(7182, 15),
	(7183, 6),
	(7184, 8),
	(7185, 12),
	(7186, 7),
	(7187, 6),
	(7188, 11),
	(7189, 6),
	(7190, 2),
	(7191, 1),
	(7192, 10),
	(7193, 15),
	(7194, 7),
	(7195, 3),
	(7196, 7),
	(7197, 15),
	(7198, 2),
	(7199, 13),
	(7200, 1),
	(7201, 4),
	(7202, 11),
	(7203, 7),
	(7204, 11),
	(7205, 10),
	(7206, 5),
	(7207, 9),
	(7208, 4),
	(7209, 2),
	(7210, 2),
	(7211, 10),
	(7212, 14),
	(7213, 3),
	(7214, 6),
	(7215, 10),
	(7216, 4),
	(7217, 6),
	(7218, 10),
	(7219, 12),
	(7220, 7),
	(7221, 11),
	(7222, 5),
	(7223, 1),
	(7224, 14),
	(7225, 13),
	(7226, 6),
	(7227, 15),
	(7228, 11),
	(7229, 12),
	(7230, 10),
	(7231, 2),
	(7232, 6),
	(7233, 9),
	(7234, 14),
	(7235, 2),
	(7236, 3),
	(7237, 4),
	(7238, 6),
	(7239, 6),
	(7240, 8),
	(7241, 1),
	(7242, 10),
	(7243, 1),
	(7244, 11),
	(7245, 13),
	(7246, 1),
	(7247, 4),
	(7248, 2),
	(7249, 14),
	(7250, 11),
	(7251, 15),
	(7252, 12),
	(7253, 10),
	(7254, 5),
	(7255, 1),
	(7256, 1),
	(7257, 8),
	(7258, 5),
	(7259, 5),
	(7260, 5),
	(7261, 9),
	(7262, 1),
	(7263, 4),
	(7264, 13),
	(7265, 8),
	(7266, 15),
	(7267, 3),
	(7268, 8),
	(7269, 5),
	(7270, 14),
	(7271, 6),
	(7272, 8),
	(7273, 8),
	(7274, 13),
	(7275, 10),
	(7276, 1),
	(7277, 13),
	(7278, 2),
	(7279, 11),
	(7280, 11),
	(7281, 8),
	(7282, 12),
	(7283, 15),
	(7284, 6),
	(7285, 2),
	(7286, 10),
	(7287, 11),
	(7288, 11),
	(7289, 14),
	(7290, 1),
	(7291, 14),
	(7292, 1),
	(7293, 5),
	(7294, 2),
	(7295, 10),
	(7296, 15),
	(7297, 12),
	(7298, 5),
	(7299, 2),
	(7300, 2),
	(7301, 15),
	(7302, 5),
	(7303, 6),
	(7304, 5),
	(7305, 8),
	(7306, 4),
	(7307, 4),
	(7308, 15),
	(7309, 4),
	(7310, 11),
	(7311, 2),
	(8000, 11),
	(8001, 6),
	(8002, 12),
	(8003, 15),
	(8004, 12),
	(8005, 1),
	(8006, 9),
	(8007, 4),
	(8008, 12),
	(8009, 15),
	(8010, 12),
	(8011, 5),
	(8012, 7),
	(8013, 15),
	(8014, 7),
	(8015, 12),
	(8016, 14),
	(8017, 10),
	(8018, 2),
	(8019, 6),
	(8020, 3),
	(8021, 11),
	(8022, 13),
	(8023, 4),
	(8024, 15),
	(8025, 6),
	(8026, 6),
	(8027, 6),
	(8028, 14),
	(8029, 4),
	(8030, 2),
	(8031, 4),
	(8032, 4),
	(8033, 2),
	(8034, 9),
	(8035, 4),
	(8036, 15),
	(8037, 9),
	(8038, 9),
	(8039, 7),
	(8040, 10),
	(8041, 6),
	(8042, 14),
	(8043, 8),
	(8044, 10),
	(8045, 3),
	(8046, 12),
	(8047, 1),
	(8048, 8),
	(8049, 15),
	(8050, 11),
	(8051, 1),
	(8052, 12),
	(8053, 9),
	(8054, 11),
	(8055, 14),
	(8056, 6),
	(8057, 12),
	(8058, 9),
	(8059, 12),
	(8060, 9),
	(8061, 3),
	(8062, 14),
	(8063, 12),
	(8064, 5),
	(8065, 14),
	(8066, 9),
	(8067, 1),
	(8068, 1),
	(8069, 1),
	(8070, 10),
	(8071, 11),
	(8072, 6),
	(8073, 2),
	(8074, 15),
	(8075, 3),
	(8076, 12),
	(8077, 13),
	(8078, 7),
	(8079, 8),
	(8080, 11),
	(8081, 3),
	(8082, 3),
	(8083, 12),
	(8084, 13),
	(8085, 2),
	(8086, 6),
	(8087, 12),
	(8088, 3),
	(8089, 6),
	(8090, 2),
	(8091, 13),
	(8092, 13),
	(8093, 7),
	(8094, 7),
	(8095, 6),
	(8096, 1),
	(8097, 12),
	(8098, 8),
	(8099, 12),
	(8100, 10),
	(8101, 1),
	(8102, 10),
	(8103, 1),
	(8104, 3),
	(8105, 2),
	(8106, 1),
	(8107, 13),
	(8108, 7),
	(8109, 8),
	(8110, 5),
	(8111, 12),
	(8112, 3),
	(8113, 14),
	(8114, 1),
	(8115, 4),
	(8116, 9),
	(8117, 8),
	(8118, 10),
	(8119, 13),
	(8120, 3),
	(8121, 4),
	(8122, 7),
	(8123, 10),
	(8124, 7),
	(8125, 14),
	(8126, 5),
	(8127, 2),
	(8128, 2),
	(8129, 13),
	(8130, 2),
	(8131, 4),
	(8132, 7),
	(8133, 4),
	(8134, 11),
	(8135, 13),
	(8136, 8),
	(8137, 2),
	(8138, 3),
	(8139, 2),
	(8140, 4),
	(8141, 10),
	(8142, 14),
	(8143, 8),
	(8144, 5),
	(8145, 6),
	(8146, 13),
	(8147, 10),
	(8148, 11),
	(8149, 5),
	(8150, 10),
	(8151, 4),
	(8152, 8),
	(8153, 6),
	(8154, 5),
	(8155, 5),
	(8156, 2),
	(8157, 10),
	(8158, 12),
	(8159, 7),
	(8160, 1),
	(8161, 10),
	(8162, 4),
	(8163, 14),
	(8164, 14),
	(8165, 15),
	(8166, 13),
	(8167, 14),
	(8168, 5),
	(8169, 15),
	(8170, 4),
	(8171, 4),
	(8172, 13),
	(8173, 7),
	(8174, 2),
	(8175, 9),
	(8176, 14),
	(8177, 1),
	(8178, 12),
	(8179, 15),
	(8180, 1),
	(8181, 5),
	(8182, 8),
	(8183, 13),
	(8184, 13),
	(8185, 4),
	(8186, 5),
	(8187, 14),
	(8188, 7),
	(8189, 10),
	(8190, 4),
	(8191, 1),
	(8192, 15),
	(8193, 4),
	(8194, 1),
	(8195, 10),
	(8196, 4),
	(8197, 14),
	(8198, 4),
	(8199, 1),
	(8200, 9),
	(8201, 5),
	(8202, 4),
	(8203, 15),
	(8204, 13),
	(8205, 13),
	(8206, 11),
	(8207, 6),
	(8208, 8),
	(8209, 11),
	(8210, 9),
	(8211, 12),
	(8212, 13),
	(8213, 8),
	(8214, 1),
	(8215, 1),
	(8216, 9),
	(8217, 11),
	(8218, 15),
	(8219, 1),
	(8220, 12),
	(8221, 9),
	(8222, 5),
	(8223, 8),
	(8224, 2),
	(8225, 8),
	(8226, 3),
	(8227, 9),
	(8228, 11),
	(8229, 12),
	(8230, 2),
	(8231, 3),
	(8232, 14),
	(8233, 2),
	(8234, 8),
	(8235, 9),
	(8236, 2),
	(8237, 10),
	(8238, 12),
	(8239, 10),
	(8240, 8),
	(8241, 8),
	(8242, 14),
	(8243, 11),
	(8244, 9),
	(8245, 3),
	(8246, 1),
	(8247, 1),
	(8248, 7),
	(8249, 8),
	(8250, 8),
	(8251, 4),
	(8252, 3),
	(8253, 4),
	(8254, 10),
	(8255, 10),
	(8256, 3),
	(8257, 10),
	(8258, 8),
	(8259, 11),
	(8260, 1),
	(8261, 7),
	(8262, 11),
	(8263, 11),
	(8264, 14),
	(8265, 14),
	(8266, 5),
	(8267, 6),
	(8268, 3),
	(8269, 5),
	(8270, 7),
	(8271, 15),
	(8272, 6),
	(8273, 15),
	(8274, 13),
	(8275, 9),
	(8276, 15),
	(8277, 2),
	(8278, 4),
	(8279, 14),
	(8280, 11),
	(8281, 1),
	(8282, 3),
	(8283, 10),
	(8284, 7),
	(8285, 5),
	(8286, 10),
	(8287, 13),
	(8288, 2),
	(8289, 9),
	(8290, 15),
	(8291, 7),
	(8292, 8),
	(8293, 10),
	(8294, 2),
	(8295, 7),
	(8296, 1),
	(8297, 3),
	(8298, 5),
	(8299, 6),
	(8300, 9),
	(8301, 4),
	(8302, 13),
	(8303, 12),
	(8304, 14),
	(8305, 10),
	(8306, 13),
	(8307, 2),
	(8308, 14),
	(8309, 6),
	(8310, 10),
	(8311, 11),
	(8312, 13),
	(8313, 11),
	(8314, 13),
	(8315, 5),
	(8316, 13),
	(8317, 2),
	(8318, 13),
	(8319, 8),
	(8320, 4),
	(8321, 12),
	(8322, 8),
	(8323, 10),
	(8324, 15),
	(8325, 12),
	(8326, 2),
	(8327, 7),
	(8328, 3),
	(8329, 11),
	(8330, 2),
	(8331, 7),
	(8332, 8),
	(8333, 7),
	(8334, 11),
	(8335, 14),
	(9000, 8),
	(9001, 1),
	(9002, 8),
	(9003, 11),
	(9004, 10),
	(9005, 2),
	(9006, 12),
	(9007, 12),
	(9008, 6),
	(9009, 7),
	(9010, 10),
	(9011, 10),
	(9012, 6),
	(9013, 9),
	(9014, 9),
	(9015, 1),
	(9016, 6),
	(9017, 4),
	(9018, 3),
	(9019, 13),
	(9020, 6),
	(9021, 7),
	(9022, 1),
	(9023, 8),
	(9024, 15),
	(9025, 14),
	(9026, 3),
	(9027, 12),
	(9028, 3),
	(9029, 2),
	(9030, 11),
	(9031, 13),
	(9032, 15),
	(9033, 3),
	(9034, 7),
	(9035, 12),
	(9036, 13),
	(9037, 12),
	(9038, 9),
	(9039, 12),
	(9040, 5),
	(9041, 5),
	(9042, 3),
	(9043, 13),
	(9044, 1),
	(9045, 6),
	(9046, 1),
	(9047, 13),
	(9048, 1),
	(9049, 13),
	(9050, 2),
	(9051, 11),
	(9052, 8),
	(9053, 12),
	(9054, 8),
	(9055, 3),
	(9056, 4),
	(9057, 8),
	(9058, 5),
	(9059, 12),
	(9060, 12),
	(9061, 3),
	(9062, 15),
	(9063, 13),
	(9064, 5),
	(9065, 15),
	(9066, 5),
	(9067, 14),
	(9068, 15),
	(9069, 2),
	(9070, 6),
	(9071, 7),
	(9072, 10),
	(9073, 3),
	(9074, 2),
	(9075, 11),
	(9076, 8),
	(9077, 9),
	(9078, 9),
	(9079, 11),
	(9080, 8),
	(9081, 7),
	(9082, 9),
	(9083, 5),
	(9084, 14),
	(9085, 15),
	(9086, 2),
	(9087, 7),
	(9088, 9),
	(9089, 10),
	(9090, 14),
	(9091, 10),
	(9092, 1),
	(9093, 8),
	(9094, 14),
	(9095, 8),
	(9096, 4),
	(9097, 14),
	(9098, 12),
	(9099, 12),
	(9100, 6),
	(9101, 15),
	(9102, 1),
	(9103, 8),
	(9104, 6),
	(9105, 11),
	(9106, 5),
	(9107, 6),
	(9108, 12),
	(9109, 11),
	(9110, 14),
	(9111, 12),
	(9112, 7),
	(9113, 15),
	(9114, 3),
	(9115, 9),
	(9116, 15),
	(9117, 11),
	(9118, 15),
	(9119, 3),
	(9120, 15),
	(9121, 1),
	(9122, 13),
	(9123, 1),
	(9124, 13),
	(9125, 5),
	(9126, 13),
	(9127, 11),
	(9128, 3),
	(9129, 7),
	(9130, 13),
	(9131, 2),
	(9132, 12),
	(9133, 1),
	(9134, 13),
	(9135, 1),
	(9136, 14),
	(9137, 14),
	(9138, 5),
	(9139, 14),
	(9140, 14),
	(9141, 9),
	(9142, 6),
	(9143, 9),
	(9144, 2),
	(9145, 13),
	(9146, 1),
	(9147, 6),
	(9148, 7),
	(9149, 4),
	(9150, 11),
	(9151, 15),
	(9152, 1),
	(9153, 3),
	(9154, 15),
	(9155, 4),
	(9156, 15),
	(9157, 3),
	(9158, 13),
	(9159, 12),
	(9160, 12),
	(9161, 1),
	(9162, 9),
	(9163, 9),
	(9164, 9),
	(9165, 12),
	(9166, 14),
	(9167, 6),
	(9168, 15),
	(9169, 5),
	(9170, 13),
	(9171, 1),
	(9172, 1),
	(9173, 13),
	(9174, 5),
	(9175, 4),
	(9176, 4),
	(9177, 14),
	(9178, 6),
	(9179, 8),
	(9180, 11),
	(9181, 11),
	(9182, 8),
	(9183, 3),
	(9184, 1),
	(9185, 1),
	(9186, 15),
	(9187, 6),
	(9188, 14),
	(9189, 1),
	(9190, 2),
	(9191, 15),
	(9192, 3),
	(9193, 12),
	(9194, 15),
	(9195, 7),
	(9196, 8),
	(9197, 3),
	(9198, 7),
	(9199, 11),
	(9200, 6),
	(9201, 5),
	(9202, 7),
	(9203, 13),
	(9204, 1),
	(9205, 3),
	(9206, 2),
	(9207, 1),
	(9208, 14),
	(9209, 12),
	(9210, 9),
	(9211, 14),
	(9212, 15),
	(9213, 13),
	(9214, 15),
	(9215, 3),
	(9216, 6),
	(9217, 1),
	(9218, 15),
	(9219, 3),
	(9220, 4),
	(9221, 11),
	(9222, 1),
	(9223, 9),
	(9224, 6),
	(9225, 4),
	(9226, 6),
	(9227, 6),
	(9228, 3),
	(9229, 9),
	(9230, 7),
	(9231, 6),
	(9232, 13),
	(9233, 1),
	(9234, 8),
	(9235, 11),
	(9236, 1),
	(9237, 11),
	(9238, 11),
	(9239, 3),
	(9240, 5),
	(9241, 7),
	(10000, 7),
	(10001, 7),
	(10002, 5),
	(10003, 10),
	(10004, 8),
	(10005, 10),
	(10006, 13),
	(10007, 1),
	(10008, 7),
	(10009, 10),
	(10010, 15),
	(10011, 12),
	(10012, 9),
	(10013, 5),
	(10014, 8),
	(10015, 9),
	(10016, 9),
	(10017, 11),
	(10018, 8),
	(10019, 9),
	(10020, 15),
	(10021, 9),
	(10022, 4),
	(10023, 9),
	(10024, 1),
	(10025, 6),
	(10026, 14),
	(10027, 6),
	(10028, 6),
	(10029, 2),
	(10030, 13),
	(10031, 11),
	(10032, 8),
	(10033, 14),
	(10034, 15),
	(10035, 8),
	(10036, 10),
	(10037, 10),
	(10038, 6),
	(10039, 13),
	(10040, 14),
	(10041, 7),
	(10042, 13),
	(10043, 8),
	(10044, 11),
	(10045, 5),
	(10046, 8),
	(10047, 9),
	(10048, 8),
	(10049, 8),
	(10050, 13),
	(10051, 11),
	(10052, 7),
	(10053, 7),
	(10054, 6),
	(10055, 10),
	(10056, 13),
	(10057, 3),
	(10058, 9),
	(10059, 7),
	(10060, 4),
	(10061, 4),
	(10062, 8),
	(10063, 5),
	(10064, 1),
	(10065, 3),
	(10066, 13),
	(10067, 3),
	(10068, 12),
	(10069, 2),
	(10070, 12),
	(10071, 8),
	(10072, 14),
	(10073, 6),
	(10074, 3),
	(10075, 6),
	(10076, 8),
	(10077, 11),
	(10078, 2),
	(10079, 2),
	(10080, 9),
	(10081, 7),
	(10082, 12),
	(10083, 11),
	(10084, 11),
	(10085, 2),
	(10086, 4),
	(10087, 8),
	(10088, 6),
	(10089, 11),
	(10090, 10),
	(10091, 2),
	(10092, 3),
	(10093, 5),
	(10094, 4),
	(10095, 15),
	(10096, 3),
	(10097, 4),
	(10098, 8),
	(10099, 4),
	(10100, 5),
	(10101, 10),
	(10102, 11),
	(10103, 1),
	(10104, 4),
	(10105, 5),
	(10106, 3),
	(10107, 12),
	(10108, 3),
	(10109, 9),
	(10110, 10),
	(10111, 15),
	(10112, 13),
	(10113, 5),
	(10114, 1),
	(10115, 3),
	(10116, 7),
	(10117, 6),
	(10118, 12),
	(10119, 5),
	(10120, 11),
	(10121, 15),
	(10122, 5),
	(10123, 4),
	(10124, 7),
	(10125, 7),
	(10126, 3),
	(10127, 4),
	(10128, 1),
	(10129, 12),
	(10130, 11),
	(10131, 11),
	(10132, 1),
	(10133, 3),
	(10134, 9),
	(10135, 6),
	(10136, 9),
	(10137, 13),
	(10138, 7),
	(10139, 5),
	(10140, 3),
	(10141, 11),
	(10142, 1),
	(10143, 7),
	(10144, 5),
	(10145, 3),
	(10146, 8),
	(10147, 1),
	(10148, 1),
	(10149, 1),
	(10150, 3),
	(10151, 3),
	(10152, 10),
	(10153, 10),
	(10154, 8),
	(10155, 5),
	(10156, 10),
	(10157, 13),
	(10158, 2),
	(10159, 9),
	(10160, 1),
	(10161, 3),
	(10162, 2),
	(10163, 3),
	(10164, 13),
	(10165, 9),
	(10166, 9),
	(10167, 2),
	(10168, 7),
	(10169, 6),
	(10170, 3),
	(10171, 4),
	(10172, 9),
	(10173, 13),
	(10174, 10),
	(10175, 14),
	(10176, 6),
	(10177, 7),
	(10178, 5),
	(10179, 9),
	(10180, 14),
	(10181, 10),
	(10182, 9),
	(10183, 7),
	(10184, 15),
	(10185, 12),
	(10186, 4),
	(10187, 14),
	(10188, 15),
	(10189, 2),
	(10190, 4),
	(10191, 13),
	(10192, 5),
	(10193, 10),
	(10194, 2),
	(10195, 8),
	(10196, 11),
	(10197, 1),
	(10198, 8),
	(10199, 14),
	(10200, 11),
	(10201, 8),
	(10202, 8),
	(10203, 12),
	(10204, 12),
	(10205, 9),
	(10206, 4),
	(10207, 10),
	(10208, 3),
	(10209, 1),
	(10210, 4),
	(10211, 8),
	(10212, 6),
	(10213, 3),
	(10214, 13),
	(10215, 14),
	(10216, 10),
	(10217, 14),
	(10218, 13),
	(10219, 4),
	(10220, 3),
	(10221, 11),
	(10222, 4),
	(10223, 9),
	(10224, 3),
	(10225, 2),
	(10226, 15),
	(10227, 4),
	(10228, 3),
	(10229, 7),
	(10230, 1),
	(10231, 8),
	(10232, 1),
	(10233, 10),
	(10234, 12),
	(10235, 10),
	(10236, 11),
	(10237, 4),
	(10238, 13),
	(10239, 14),
	(10240, 9),
	(10241, 7),
	(10242, 14),
	(10243, 7),
	(10244, 2),
	(10245, 1),
	(10246, 6),
	(10247, 8),
	(10248, 1),
	(10249, 8),
	(10250, 2),
	(10251, 15),
	(10252, 7),
	(10253, 14),
	(10254, 14),
	(10255, 7),
	(10256, 9),
	(10257, 10),
	(10258, 4),
	(10259, 2),
	(10260, 11),
	(10261, 9),
	(10262, 8),
	(10263, 1),
	(10264, 6),
	(10265, 3),
	(10266, 14),
	(10267, 15),
	(10268, 13),
	(10269, 13),
	(10270, 13),
	(10271, 3),
	(10272, 5),
	(10273, 7),
	(10274, 6),
	(10275, 11),
	(10276, 9),
	(10277, 5),
	(10278, 4),
	(10279, 4),
	(10280, 4),
	(10281, 12),
	(10282, 5),
	(10283, 6),
	(10284, 3),
	(10285, 15),
	(10286, 8),
	(10287, 10),
	(10288, 7),
	(10289, 8),
	(10290, 1),
	(10291, 4),
	(10292, 11),
	(10293, 9),
	(10294, 1),
	(10295, 12),
	(10296, 15),
	(10297, 1),
	(10298, 15),
	(10299, 3),
	(10300, 12),
	(10301, 1),
	(10302, 1),
	(10303, 8),
	(10304, 9),
	(10305, 1),
	(10306, 3),
	(10307, 7),
	(10308, 4),
	(10309, 5),
	(10310, 8),
	(10311, 1),
	(10312, 4),
	(10313, 12),
	(10314, 14),
	(10315, 8),
	(10316, 3),
	(10317, 7),
	(10318, 10),
	(10319, 10),
	(10320, 15),
	(10321, 3),
	(10322, 5),
	(10323, 3),
	(10324, 6),
	(10325, 14),
	(10326, 10),
	(10327, 9),
	(10328, 11),
	(10329, 6),
	(10330, 11),
	(10331, 4),
	(10332, 12),
	(10333, 4),
	(10334, 10),
	(10335, 15),
	(11000, 10),
	(11001, 4),
	(11002, 11),
	(11003, 4),
	(11004, 15),
	(11005, 3),
	(11006, 13),
	(11007, 5),
	(11008, 12),
	(11009, 11),
	(11010, 11),
	(11011, 2),
	(11012, 8),
	(11013, 5),
	(11014, 12),
	(11015, 11),
	(11016, 4),
	(11017, 14),
	(11018, 5),
	(11019, 7),
	(11020, 1),
	(11021, 15),
	(11022, 8),
	(11023, 1),
	(11024, 12),
	(11025, 1),
	(11026, 14),
	(11027, 3),
	(11028, 14),
	(11029, 15),
	(11030, 13),
	(11031, 14),
	(11032, 11),
	(11033, 5),
	(11034, 5),
	(11035, 6),
	(11036, 14),
	(11037, 10),
	(11038, 15),
	(11039, 3),
	(11040, 6),
	(11041, 9),
	(11042, 13),
	(11043, 4),
	(11044, 11),
	(11045, 12),
	(11046, 1),
	(11047, 4),
	(11048, 1),
	(11049, 7),
	(11050, 5),
	(11051, 1),
	(11052, 4),
	(11053, 13),
	(11054, 12),
	(11055, 1),
	(11056, 7),
	(11057, 6),
	(11058, 7),
	(11059, 14),
	(11060, 8),
	(11061, 4),
	(11062, 11),
	(11063, 9),
	(11064, 14),
	(11065, 10),
	(11066, 15),
	(11067, 12),
	(11068, 10),
	(11069, 8),
	(11070, 8),
	(11071, 1),
	(11072, 13),
	(11073, 7),
	(11074, 9),
	(11075, 15),
	(11076, 8),
	(11077, 7),
	(11078, 9),
	(11079, 8),
	(11080, 14),
	(11081, 13),
	(11082, 14),
	(11083, 3),
	(11084, 1),
	(11085, 10),
	(11086, 5),
	(11087, 14),
	(11088, 1),
	(11089, 10),
	(11090, 13),
	(11091, 9),
	(11092, 11),
	(11093, 5),
	(11094, 1),
	(11095, 7),
	(11096, 13),
	(11097, 5),
	(11098, 8),
	(11099, 8),
	(11100, 1),
	(11101, 5),
	(11102, 11),
	(11103, 7),
	(11104, 12),
	(11105, 2),
	(11106, 7),
	(11107, 11),
	(11108, 10),
	(11109, 5),
	(11110, 13),
	(11111, 3),
	(11112, 3),
	(11113, 2),
	(11114, 2),
	(11115, 2),
	(11116, 1),
	(11117, 1),
	(11118, 1),
	(11118, 2),
	(11119, 1),
	(11119, 2),
	(11120, 1),
	(11120, 2);
/*!40000 ALTER TABLE `books_publishing_companies` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
