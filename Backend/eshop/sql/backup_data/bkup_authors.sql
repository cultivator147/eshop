-- --------------------------------------------------------
-- Host:                         103.145.63.98
-- Server version:               10.5.20-MariaDB-log - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table hieutm.authors
CREATE TABLE IF NOT EXISTS `authors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Dumping data for table hieutm.authors: ~15 rows (approximately)
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` (`id`, `name`, `nationality`, `year`) VALUES
	(1, 'Trịnh Minh Hiếu', 'Viet Nam', 2002),
	(2, '\n                             Nhiều Tác Giả \n                        ', 'Việt Nam', 2022),
	(3, '\n                            GS Phan Văn Trường, Nhiều tác giả\n                        ', 'Việt Nam', 2022),
	(4, '\n                            Nhiều  Tác Giả\n                        ', 'Việt Nam', 2022),
	(5, '\n                            Nhiều Tác Giả\n                        ', 'Việt Nam', 2022),
	(6, '\n                            Nhiều Tác Giả Từ Group Thìa Đầy Thơ\n                        ', 'Việt Nam', 2022),
	(7, '\n                            Nhóm tác giả \n                        ', 'Việt Nam', 2022),
	(8, '\n                            Nhóm tác giả Heary\n                        ', 'Việt Nam', 2022),
	(9, '\n                            Nhóm Tác Giả LoveBook\n                        ', 'Việt Nam', 2022),
	(10, '\n                            Nhóm tác giả NA9\n                        ', 'Việt Nam', 2022),
	(11, '\n                            Nhóm tác giả Trần Công Diêu\n                        ', 'Việt Nam', 2022),
	(12, '\n                            Nhóm tác giả và họa sĩ Disney\n                        ', 'Việt Nam', 2022),
	(13, '\n                            Nhóm tác giả VNHR\n                        ', 'Việt Nam', 2022),
	(14, '\n                            Tác giả Việt\n                        ', 'Việt Nam', 2022),
	(15, '\n                            Tác giả Việt\n                        ', 'Việt Nam', 2022);
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
