-- --------------------------------------------------------
-- Host:                         103.145.63.98
-- Server version:               10.5.20-MariaDB-log - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table hieutm.publishing_companies
CREATE TABLE IF NOT EXISTS `publishing_companies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Dumping data for table hieutm.publishing_companies: ~17 rows (approximately)
/*!40000 ALTER TABLE `publishing_companies` DISABLE KEYS */;
INSERT INTO `publishing_companies` (`id`, `address`, `name`) VALUES
	(1, 'Tạ Quang Bửu, Bách Khoa, Hà Nội', 'NXB Bách Khoa'),
	(2, 'Đại học Bách Khoa', 'HUST'),
	(3, 'Hà Nội', 'NXB Kim Đồng'),
	(4, 'Hà Nội', 'NXB ĐH Quốc Gia Hà Nội'),
	(5, 'Hồ Chí Minh', 'NXB Phụ Nữ'),
	(6, 'Hồ Chí Minh', 'Lao Động - Xã Hội'),
	(7, 'Hồ Chí Minh', 'NXB Dân Trí'),
	(8, 'Hồ Chí Minh', 'Nhà Xuất Bản Lao động'),
	(9, 'Hồ Chí Minh', 'NXB Tổng Hợp TPHCM'),
	(10, 'Hồ Chí Minh', 'NXB Thế Giới'),
	(11, 'Hồ Chí Minh', 'NXB Mỹ Thuật'),
	(12, 'Hồ Chí Minh', 'NXB Thanh Niên'),
	(13, 'Hồ Chí Minh', 'NXB Hải Phòng'),
	(14, 'Hà Nội', 'NXB Tài Chính'),
	(15, 'Hà Nội', 'Phụ Nữ Việt Nam'),
	(16, 'Đồng Nai', 'NXB Đồng Nai'),
	(17, 'Hà Nội', 'NXB Từ Điển Bách Khoa');
/*!40000 ALTER TABLE `publishing_companies` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
